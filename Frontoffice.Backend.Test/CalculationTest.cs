using Xunit;
using CRM.Data.Models;
using Frontoffice.Backend.Services;
using CRM.Data.Models;

namespace Frontoffice.Backend.Test
{
    public class CalculationTest
    {
        private (Application application, decimal premium, decimal sum) CreateTravel1()
        {
            var tData = new Application();
            tData.Programs = new ApplicationProgram[] { new ApplicationProgram() {
                Limits = new ApplicationLimit[] { 
                new ApplicationLimit(){
                    Title = "Медицинские услуги",
                    IsLimitSum = true,
                    IsLimitPremium = false,
                    LimitSum = 10000
                },
                new ApplicationLimit(){
                    Title = "Медико-транспортные услуги",
                    IsLimitSum = true,
                    IsLimitPremium = false,
                    LimitSum = 4000
                },
                new ApplicationLimit(){
                    Title = "Иные услуги",
                    IsLimitSum = true,
                    IsLimitPremium = false,
                    LimitSum = 5000
                },
                new ApplicationLimit(){
                    Title = "Страхование от несчастных случаев",
                    IsLimitSum = true,
                    IsLimitPremium = false,
                    LimitSum = 1000
                },
                new ApplicationLimit(){
                    Title = "Базовая страховая премия",
                    IsLimitSum = false,
                    IsLimitPremium = true,
                    LimitPremium = 1
                }
                },
                Rates = new ApplicationRate[] {
                    new ApplicationRate(){
                        Title = "Возрастной тариф",
                        Premium = 1.0m
                    },
                    new ApplicationRate(){
                        Title = "Целевой тариф",
                        Premium = 1.0m
                    },
                    new ApplicationRate(){
                        Title = "Групповой тариф",
                        Premium = 1.0m
                    },
                    new ApplicationRate(){
                        Title = "Длительность путешествия",
                        Premium = 15.0m
                    },
                    new ApplicationRate(){
                        Title = "Количество участников",
                        Premium = 2.0m
                    },
                    new ApplicationRate(){
                        Title = "Базовый тариф",
                        Premium = 0.47m
                    }
                },
            }};
            tData.InsurancePremiumRate = null;
            tData.InsuranceSumRate = new ApplicationRate()
            {
                Title = "Общий тариф связанный со страховой суммой программы",
                Premium = 1.0m
            };
            return (tData, 14.10m, 20000);
        }
        
        [Fact]
        public void TestTravel1()
        {
            (var tData, var p, var s) = CreateTravel1(); 

            var result = ApplicationCalculationProgram.Calculate(tData);
            Assert.True(result.success);
            Assert.Equal(p, result.premium);
            Assert.Equal(s, result.sum);
        }

        private (Application application, decimal premium, decimal sum) CreateOsagoUnlim()
        {
            var tData = new Application();
            tData.Programs = new ApplicationProgram[] { new ApplicationProgram() {
                Limits = new ApplicationLimit[] {
                new ApplicationLimit(){
                    Title = "Страховая сумма",
                    IsLimitSum = true,
                    IsLimitPremium = false,
                    LimitSum = 40000000
                },
                new ApplicationLimit(){
                    Title = "Страховая премия",
                    IsLimitSum = false,
                    IsLimitPremium = true,
                    LimitPremium = 40000000
                }
                },
                Rates = new ApplicationRate[] {
                    new ApplicationRate(){
                        Title = " - г. Ташкент и Ташкентская область",
                        Premium = 1.4m
                    },
                    new ApplicationRate(){
                        Title = " - легковые автомобили (A)",
                        Premium = 0.1m
                    },
                    new ApplicationRate(){
                        Title = " - не ограничено",
                        Premium = 3m
                    }
                },
            }};
            tData.InsurancePremiumRate = new ApplicationRate()
            {
                Title = "Общий тариф связанный со страховой премией программы",
                Premium = 0.01m
            }; 
            tData.InsuranceSumRate = new ApplicationRate()
            {
                Title = "Общий тариф связанный со страховой суммой программы",
                Premium = 1m
            };
            return (tData, 168000, 40000000);
        }
        [Fact]
        public void TestOsagoUnlim()
        {
            (var tData, var p, var s) = CreateOsagoUnlim();

            var result = ApplicationCalculationProgram.Calculate(tData);
            Assert.True(result.success);
            Assert.Equal(p, result.premium);
            Assert.Equal(s, result.sum);
        }

        private (Application application, decimal premium, decimal sum) CreateOsagoUpTo5()
        {
            var tData = new Application();
            tData.Programs = new ApplicationProgram[] { new ApplicationProgram() {
                Limits = new ApplicationLimit[] {
                new ApplicationLimit(){
                    Title = "Страховая сумма",
                    IsLimitSum = true,
                    IsLimitPremium = false,
                    LimitSum = 40000000
                },
                new ApplicationLimit(){
                    Title = "Страховая премия",
                    IsLimitSum = false,
                    IsLimitPremium = true,
                    LimitPremium = 40000000
                }
                },
                Rates = new ApplicationRate[] {
                    new ApplicationRate(){
                        Title = " - г. Ташкент и Ташкентская область",
                        Premium = 1.4m
                    },
                    new ApplicationRate(){
                        Title = " - легковые автомобили (A)",
                        Premium = 0.1m
                    },
                    new ApplicationRate(){
                        Title = " - до 5 чел",
                        Premium = 1m
                    }
                },
            }};
            tData.InsurancePremiumRate = new ApplicationRate()
            {
                Title = "Общий тариф связанный со страховой премией программы",
                Premium = 0.01m
            };
            tData.InsuranceSumRate = new ApplicationRate()
            {
                Title = "Общий тариф связанный со страховой суммой программы",
                Premium = 1.0m
            };
            return (tData, 56000, 40000000);
        }
        [Fact]
        public void TestOsagoUpTo5()
        {
            (var tData, var p, var s) = CreateOsagoUpTo5();

            var result = ApplicationCalculationProgram.Calculate(tData);
            Assert.True(result.success);
            Assert.Equal(p, result.premium);
            Assert.Equal(s, result.sum);
        }
        private (Application application, decimal premium, decimal sum) CreateOsgor1()
        {
            var tData = new Application();
            tData.Programs = new ApplicationProgram[] { new ApplicationProgram() {
                Limits = new ApplicationLimit[] {
                new ApplicationLimit(){
                    Title = "Страховая сумма",
                    IsLimitSum = true,
                    IsLimitPremium = false,
                    LimitSum = 82800000
                },
                new ApplicationLimit(){
                    Title = "Страховая премия",
                    IsLimitSum = false,
                    IsLimitPremium = true,
                    LimitPremium = 82800000
                }
                },
                Rates = new ApplicationRate[] {
                    new ApplicationRate(){
                        Title = " Классификация профессионального риска  - класс 01",
                        Premium = 0.571m
                    },
                    new ApplicationRate(){
                        Title = " 100%",
                        Premium = 0.01m
                    }
                },
            }};
            tData.InsurancePremiumRate = new ApplicationRate()
            {
                Title = "Общий тариф связанный со страховой премией программы",
                Premium = 0.1m
            };
            tData.InsuranceSumRate = new ApplicationRate()
            {
                Title = "Общий тариф связанный со страховой суммой программы",
                Premium = 1m
            };
            return (tData, 47278.80m, 82800000);
        }
        [Fact]
        public void TestOsgor1()
        {
            (var tData, var p, var s) = CreateOsgor1();

            var result = ApplicationCalculationProgram.Calculate(tData);
            Assert.True(result.success);
            Assert.Equal(p, result.premium);
            Assert.Equal(s, result.sum);
        }
    }
}