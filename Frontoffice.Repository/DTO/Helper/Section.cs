﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class Section
    {
        public long? Id { get; set; } = null;
        public bool IsIncluded { get; set; } = false;
        public bool IsRequired { get; set; } = false;
        public bool IsInherited { get; set; } = false;
        public bool IsMultiple { get; set; } = false;
        public bool IsSalesOptioned { get; set; } = false;
    }
}
