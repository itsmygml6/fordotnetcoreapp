﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM.Data.Enums;
using CRM.Data.Models;
using Newtonsoft.Json;
using PaymentSourceType = CRM.Data.Enums.PaymentSourceType;

namespace Frontoffice.Repository.DTO
{
    public class ApplicationCreationDTO
    {
        public long? Id { get; set; } = 0;
        public string? DocumentUID { get; set; }
        public string? TemplateUID { get; set; }
        public decimal InsuranceSum { get; set; }
        public decimal InsurancePremium { get; set; }
        public DateTime EffectStart { get; set; }
        public DateTime EffectEnd { get; set; }
        public DateTime InsuranceStart { get; set; }
        public DateTime InsuranceEnd { get; set; }
        public State State { get; set; } = State.Applied;
        public long? InsurancePremiumCurrencyId { get; set; }
        public long? InsuranceSumCurrencyId { get; set; }
        public long? ProductTemplateId { get; set; }
        
        public ICollection<ApplicationParticipant>? Participants { get; set; }
        public ICollection<ApplicationObject>? Objects { get; set; }
        public ICollection<ApplicationProgram>? Programs { get; set; } = null;
        public ICollection<ApplicationRisk>? Risks { get; set; }
        public ICollection<ApplicationLimit>? Limits { get; set; }
        public ICollection<ApplicationRate>? Rates { get; set; }
        public PaymentSourceType? PaymentSourceType { get; set; }
        public ICollection<AttachmentType>? Attachments { get; set; }
        public ICollection<DocumentTemplate>? DocumentTemplates { get; set; }
        public InsuranceSchedule? InsuranceSchedule { get; set; }
    }
}
