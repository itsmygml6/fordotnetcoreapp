﻿using CRM.Data.Enums;
using CRM.Data.Models;
using Frontoffice.Repository.DTO.Sections;
using Newtonsoft.Json;

namespace Frontoffice.Repository.DTO
{
    public class ApplicationIncomingDTO
    {
        public string? DocumentUID { get; set; }
        public string? TemplateUID { get; set; }
        public decimal InsuranceSum { get; set; }
        public decimal InsurancePremium { get; set; }
        public DateTime EffectStart { get; set; }
        public DateTime EffectEnd { get; set; }
        public DateTime InsuranceStart { get; set; }
        public DateTime InsuranceEnd { get; set; }
        public State State { get; set; } = State.Applied;
        public long? InsurancePremiumCurrencyId { get; set; }
        public long? InsuranceSumCurrencyId { get; set; }
        public long? ProductTemplateId { get; set; }
        public string ExtraData { get; set; } = String.Empty;
        
        public ICollection<ApplicationParticipantDTO>? Participants { get; set; }
        public ApplicationRateSectionDTO? RatesSection { get; set; }
        public ApplicationLimitSectionDTO? LimitSection { get; set; }
        public ApplicationRiskSectionDTO? RisksSection { get; set; }
        public ApplicationObjectSectionDTO? InsuranceObjectSection { get; set; }
        
        public PaymentSourceTypeDTO? PaymentSourceType { get; set; }
        public InsuranceScheduleDTO? InsuranceSchedule { get; set; }
        public ICollection<StageDTO>? Stages { get; set; }
    }
}
