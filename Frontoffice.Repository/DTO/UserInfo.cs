﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class UserInfo
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public object Password { get; set; }
        public bool TempPassword { get; set; }
        public string WorkPhone { get; set; }
        public string CellPhone { get; set; }
        public string Role { get; set; }
        public bool IsBlocked { get; set; }
    }


}
