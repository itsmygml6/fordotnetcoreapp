﻿using CRM.Data.Models;

namespace Frontoffice.Repository.DTO
{
    public class ApplicationRiskDTO
    {
        public long? Id { get; set; }
        public string Title { get; set; }
        public virtual ICollection<ApplicationRiskExclusionsDTO>? Exclusions { get; set; }
    }
}
