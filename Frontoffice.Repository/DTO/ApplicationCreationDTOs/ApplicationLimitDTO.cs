﻿using CRM.Data.Models;

namespace Frontoffice.Repository.DTO
{
    public class ApplicationLimitDTO
    {
        public string Title { get; set; }
        public bool IsSubLimit { get; set; }
        public ApplicationLimitDTO? Parent { get; set; }
        public bool IsLimitSum { get; set; }
        public decimal? LimitSum { get; set; }
        public bool IsSumPercent { get; set; }
        public bool IsLimitPremium { get; set; }
        public decimal? LimitPremium { get; set; }
        public bool IsPremiumPercent { get; set; }
        public bool IsDeductible { get; set; }
        public decimal? LimitDeductible { get; set; }
        public bool IsDeductiblePercent { get; set; }
        
        public ICollection<ApplicationLimitDTO>? SubLimits { get; set; }
        public ApplicationRateDTO? Rate { get; set; }
    }
}
