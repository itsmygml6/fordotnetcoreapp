﻿using CRM.Data.Enums;
using CRM.Data.Models;

namespace Frontoffice.Repository.DTO
{
    public class StageDTO
    {
        public StageType StageType { get; set; }
        public bool IsIncluded { get; set; } = true;
        public int Order { get; set; }
        public virtual ICollection<DocumentTemplateDTO>? Templates { get; set; }
        public virtual ICollection<AttachmentDTO>? Attachments { get; set; }
    }
}
