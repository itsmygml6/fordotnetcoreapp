﻿using CRM.Data.Models;

namespace Frontoffice.Repository.DTO
{
    public class ApplicationInsuranceObjectDTO
    {
        public long? Id { get; set; }
        public string Title { get; set; } 
        public string? Description { get; set; }
        public decimal DeclaredCost { get; set; }
        public decimal InsuranceCost { get; set; }
        public decimal Quantity { get; set; }
        public virtual ICollection<ObjectDetailsTemplateDTO>? ObjectDetails { get; set; }
    }
}
