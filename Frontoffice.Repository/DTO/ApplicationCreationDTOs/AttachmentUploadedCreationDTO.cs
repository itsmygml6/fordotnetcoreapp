﻿namespace Frontoffice.Repository.DTO
{
    public class AttachmentUploadedCreationDTO
    {
        public string Title { get; set; }
        public string Name { get; set; }
    }
}
