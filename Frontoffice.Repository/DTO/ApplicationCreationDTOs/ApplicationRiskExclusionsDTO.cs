﻿namespace Frontoffice.Repository.DTO
{
    public class ApplicationRiskExclusionsDTO
    {
        public long? Id { get; set; }
        public string Title { get; set; }
    }
}
