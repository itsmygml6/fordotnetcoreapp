﻿using CRM.Data.Models;

namespace Frontoffice.Repository.DTO
{
    public class ApplicationObjectSectionDTO : Section
    {
        public ICollection<ApplicationInsuranceObjectDTO> Objects { get; set; }
    }
}
