﻿using CRM.Data.Models;

namespace Frontoffice.Repository.DTO.Sections
{
    public class ApplicationLimitSectionDTO : Section
    {
        public ICollection<ApplicationLimitDTO> Limits { get; set; }
    }
}
