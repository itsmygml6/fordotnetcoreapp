﻿namespace Frontoffice.Repository.DTO.Sections
{
    public class ApplicationRateSectionDTO : Section
    {
        public ICollection<ApplicationRateDTO> Rates { get; set; }
    }
}
