﻿namespace Frontoffice.Repository.DTO.Sections
{
    public class ApplicationRiskSectionDTO : Section
    {
        public ICollection<ApplicationRiskDTO> Risks { get; set; }
    }
}
