﻿using Type = CRM.Data.Enums.Type;

namespace Frontoffice.Repository.DTO
{
    public class DocumentTemplateDTO
    {
        public long? Id { get; set; }
        public Type Type { get; set; } = Type.Other;
        public bool IsIncluded { get; set; } = true;
        public bool IsRequired { get; set; } = true;
        public virtual List<DocumentFileDTO>? Files { get; set; }
    }
}
