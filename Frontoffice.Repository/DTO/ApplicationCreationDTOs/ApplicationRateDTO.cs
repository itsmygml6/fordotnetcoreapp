﻿namespace Frontoffice.Repository.DTO
{
    public class ApplicationRateDTO
    {
        public long? Id { get; set; }
        public string Title { get; set; }
        public decimal? Premium { get; set; }
    }
}
