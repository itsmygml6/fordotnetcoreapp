﻿using CRM.Data.Enums;

namespace Frontoffice.Repository.DTO
{
    public class ApplicationParticipantDTO
    {
        public RoleType Role { get; set; }
        public long CounteragentId { get; set; }
    }
}
