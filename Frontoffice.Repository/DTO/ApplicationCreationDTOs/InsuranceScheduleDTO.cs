﻿namespace Frontoffice.Repository.DTO
{
    public class InsuranceScheduleDTO
    {
        public DateTime EffectDurationFrom { get; set; }
        public DateTime EffectDurationTo { get; set; }
        public bool IsInsuranceDurationFixed { get; set; }
        public DateTime InsuranceDurationFrom { get; set; }
        public DateTime InsuranceDurationTo { get; set; }
        public bool IsEffectDurationExtended { get; set; }
        public long EffectDurationExtensionDays { get; set; }
    }
}
