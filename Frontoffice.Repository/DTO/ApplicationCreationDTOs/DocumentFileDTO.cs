﻿using CRM.Data.Enums;

namespace Frontoffice.Repository.DTO
{
    public class DocumentFileDTO
    {
        public long? Id { get; set; }
        public string Title { get; set; } = null!;
        public string Template { get; set; }
        public Languages Languages { get; set; } = Languages.UzbLat;
        public InsurantType InsurantType { get; set; } = InsurantType.Any;
    }
}
