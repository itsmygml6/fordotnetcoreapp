﻿using CRM.Data.Models;

namespace Frontoffice.Repository.DTO
{
    public class PaymentSourceTypeDTO
    {
        public string Title { get; set; } = null!;
        public PaymentTermDTO? PaymentTerm { get; set; }
    }
}
