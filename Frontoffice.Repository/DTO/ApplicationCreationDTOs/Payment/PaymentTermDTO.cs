﻿using CRM.Data.Enums;
using CRM.Data.Models;

namespace Frontoffice.Repository.DTO
{
    public class PaymentTermDTO
    {
        public long? InsurancePremiumCurrencyId { get; set; }
        public bool RecalculatePremiun { get; set; } = true;
        public long? InsuranceLiabilitiesCurrencyId { get; set; }
        public bool RecalculateLiabilitiesCurrency { get; set; } = true;
        public PaymentTermType PaymentTermType { get; set; }
        
        public virtual ICollection<PaymentEventDTO>? PaymentTermEvents { get; set; }
        public virtual ICollection<PaymentPeriodDTO>? PaymentTermPeriods { get; set; }
    }
}
