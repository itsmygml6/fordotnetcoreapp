﻿using CRM.Data.Enums;

namespace Frontoffice.Repository.DTO
{
    public class PaymentPeriodDTO
    {
        public int PaymentPeriods { get; set; }
        public PeriodType PeriodType { get; set; }
        public bool IsPremiumSplitted { get; set; } = false;
        public int PremiumParts { get; set; } = 0;
    }
}
