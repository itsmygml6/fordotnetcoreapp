﻿namespace Frontoffice.Repository.DTO
{
    public class ObjectDetailsTemplateDTO
    {
        public long? Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
    }
}
