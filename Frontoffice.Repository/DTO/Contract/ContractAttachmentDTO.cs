﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class ContractAttachmentDTO : AttachmentDTO
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public long? Size { get; set; }
        public string? Extension { get; set; }
        public string? Path { get; set; }
    }
}
