﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class ContractCreateDTO : ContractDTO
    {
        public List<AttachmentDTO>? DeletedAttachments { get; set; }
    }
}
