﻿using CRM.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class ContractDTO
    {
        public int? Id { get; set; }
        public ContractType ContractType { get; set; }
        public ContractState State { get; set; }
        public string RegistrationNumber { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime DueDate { get; set; }
        public string DealNumber { get; set; }
        public decimal Sum { get; set; }
        public long? CurrencyId { get; set; }
        public string SignerOwnId { get; set; }
        public string SignerOwn { get; set; }
        public long CounteragentId { get; set; }
        public string Counteragent { get; set; }
        public ICollection<ContractAttachmentDTO>? Attachments { get; set; }
    }
}
