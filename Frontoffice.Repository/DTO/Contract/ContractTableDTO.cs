﻿using CRM.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class ContractTableDTO
    {
        public int? Id { get; set; }
        public string RegistrationNumber { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime DueDate { get; set; }
        public string DealNumber { get; set; }
        public decimal Sum { get; set; }
        public long? CurrenceId { get; set; }
        public string State { get; set; }
    }
}
