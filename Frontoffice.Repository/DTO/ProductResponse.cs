﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class ProductResponse
    {
        public long Id { get; set; }
        public string Title { get; set; } = null!;
        public string? Description { get; set; } 
        public string Type { get; set; } = null!;
        public string[]? Classes { get; set; }
        public string[]? Stages { get; set; }
        public decimal? TotalInsuranceSum { get; set; }
        public decimal? TotalInsurancePremium { get; set; }
    }
}
