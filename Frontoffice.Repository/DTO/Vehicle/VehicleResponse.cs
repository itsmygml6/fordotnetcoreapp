﻿using CRM.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class VehicleResponse
    {
        public string GovNumber { get; set; }
        public string VehicleLicenceNumber { get; set; }
        public string VehicleLicenceSeria { get; set; }
        public string ModelName { get; set; }
        public VehicleType VehicleTypeId { get; set; }
        public string Division { get; set; }
        public string IssueYear { get; set; }
        public string BodyNumber { get; set; }
        public string EngineNumber { get; set; }
        public string Owner { get; set; }
        public string VehicleColor { get; set; }
        public string TechPassportIssueDate { get; set; }
        public int? RegionId { get; set; }

    }
}
