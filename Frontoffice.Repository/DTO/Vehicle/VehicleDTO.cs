﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class VehicleDTO
    {
        public string govNumber { get; set; }
        public string techPassportSeria{ get; set; }
        public string techPassportNumber { get; set; }
    }
}
