﻿using CRM.Data.Enums;

namespace Frontoffice.Repository.DTO
{
    public class LimitDTO
    {
        public string Title { get; set; } = null!;
        public long? SectionId { get; set; }
        public bool IsSubLimit { get; set; }
        public bool IsConnected { get; set; } = false;
        public LimitConnectionType ConnectionType { get; set; }
        public bool IsLimitSum { get; set; } = false;
        public decimal? LimitSumMin { get; set; }
        public decimal? LimitSumMax { get; set; }
        public bool IsSumPercent { get; set; } = false;
        public bool IsLimitPremium { get; set; } = false;
        public decimal? LimitPremiumMin { get; set; }
        public decimal? LimitPremiumMax { get; set; }
        public bool IsPremiumPercent { get; set; } = false;
        public bool IsDeductible { get; set; } = false;
        public decimal? DeductibleMin { get; set; }
        public decimal? DeductibleMax { get; set;}
        public bool IsDeductiblePercent { get; set;} = false; 
        public decimal? LimitSum { get; set; }
        public decimal? LimitPremium { get; set; }
        public decimal? LimitDeductible { get; set; }
        public bool IsFixed { get; set; } = true;
        public bool IsRequired { get; set; } = true;
        public virtual ICollection<LimitDTO>? SubLimits { get; set; }
    }
}

