﻿using CRM.Data.Models;

namespace Frontoffice.Repository.DTO
{
    public class LimitValidationDTO
    {
        public LimitDTO Limit { get; set; }
        public long Product { get; set; }
    }
}
