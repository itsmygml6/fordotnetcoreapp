﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM.Data.Enums;
using CRM.Data.Enums.Bills;

namespace Frontoffice.Repository.DTO.Billing
{
    public class BillCreation
    {
        public DateTime IssuedDate { get; set; }

        public string Number { get; set; }

        public decimal ChargedSum { get; set; }

        //public Currency Currency { get; set; }

        //public CurrencyRates CurrencyRates { get; set; }

        public DateTime DueDate { get; set; }

        public decimal PayedSum { get; set; }

        public DateTime PayedDate { get; set; }
        
        public BillState State { get; set; }
        public long? AttachmentId { get; set; }
        public long? PaymentSourceTypeId { get; set; }
        
        /// <summary>
        /// Payme test
        /// </summary>
        public TransactionResult TransactionResult { get; set; }
        public ChequeState ChequeState { get; set; }
        public string? TransactionId { get; set; }
        public string? ReceiptId { get; set; }
        public long CreateTransactionTime { get; set; } = 0;
        public long PerformTransactionTime { get; set; } = 0;
        public long CancelTransactionTime { get; set; } = 0;
        
        public long? ApplicationId { get; set; }
    }
}
