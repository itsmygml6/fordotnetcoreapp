﻿namespace Frontoffice.Repository.DTO
{
    public class PayBillDTO
    {
        public long BillId { get; set; }
        public long ChargedSum { get; set; }
        public string PhoneNumber { get; set; }
        public string Url { get; set; } = "";
    }
}
