﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class BillDTO
    {
        public long Id { get; set; }
        public DateTime IssuedDate { get; set; }
        public string Number { get; set; }
        public decimal ChargedSum { get; set; }
        public long? Currency { get; set; } // Handbook
        public long? CurrencyRateId { get; set; } // Handbook
        public DateTime DueDate { get; set; }
        public decimal PayedSum { get; set; }
        public DateTime PayedDate { get; set; }
        public string State { get; set; }
        public string? TransactionResult { get; set; }
        public string ChequeState { get; set; }
        public string ReceiptId { get; set; }
        public string TransactionId { get; set; }
        public long CreateTransactionTime { get; set; } = 0;
        public long PerformTransactionTime { get; set; } = 0;
        public long CancelTransactionTime { get; set; } = 0;
        public string Status { get; set; }
        public long? AttachmentId { get; set; }
        public long? PaymentSourceTypeId { get; set; }
        public long? ApplicationId { get; set; }
    }
}
