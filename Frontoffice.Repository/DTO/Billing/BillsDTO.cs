﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class BillsDTO
    {
        public long Id { get; set; }
        public string? DealNo { get; set; }
        public string? BillNo { get; set; }
        public decimal ChargedPremium { get; set; }
        public DateTime ChargedDate { get; set; }
        public DateTime DueDate { get; set; }
        public decimal PaidPremium { get; set; }
        public DateTime PaidDate { get; set; }
        public string Status { get; set; }
        public string State { get; set; }
    }
}
