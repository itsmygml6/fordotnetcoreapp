﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    /// <summary>
    /// Select endpoint va id, title kerak bo`lgan qismlar uchun
    /// </summary>
    public class Select
    {
        public long Id { get; set; }
        public string Title { get; set; } = null!;
    }
}
