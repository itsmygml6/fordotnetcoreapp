﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class DealForCreateContractDTO
    {
        public string DealNumber { get; set; }
        public string SellerEmployeeId { get; set; }
        public long? SellerCounteragentId { get; set; }
        public string SellerEpmloyeeName { get; set; }
        public string SellerCounteragentName { get; set; }
    }
}
