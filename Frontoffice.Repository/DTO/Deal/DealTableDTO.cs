﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class DealTableDTO
    {
        public long Id { get; set; }
        public string Number { get; set; }
        public string ProductName { get; set; }
        public string Client { get; set; }
        public string TotalProductSum { get; set; }
    }
}
