﻿using CRM.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class DealDTO
    {
        public long Id { get; set; }
        public string Seller { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string ClientPhone { get; set; }
        public string InsuranceProduct { get; set; }
        public decimal InsuranceSum { get; set; }
        public decimal InsurancePremium { get; set; }
        public DateTime ContractDurationStart { get; set; }
        public DateTime ContractDurationEnd { get; set; }
        public DateTime InsuranceDurationStart { get; set; }
        public DateTime InsuranceDurationEnd { get; set; }
        public SalesDealStages Stage { get; set; }
        public string ApplicationNumber { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string ContractNumber { get; set; }
        public DateTime ContractDate { get; set; }
        public string PolicySerial { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime PolicyDate { get; set; }
    }
}
