﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class PersonResponse
    {
        public string Pinfl { get; set; }
        public string Document { get; set; }
        public string LastNameLatin { get; set; }
        public string FirstNameLatin { get; set; }
        public string MiddleNameLatin { get; set; }
        public string LastNameEng { get; set; }
        public string FirstEng { get; set; }
        public string BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string BirthCountry { get; set; }
        public string Gender { get; set; }
        public string IssuedBy { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int RegionId { get; set; }
        public int DistrictId { get; set; }
        public string Address { get; set; }

    }
}
