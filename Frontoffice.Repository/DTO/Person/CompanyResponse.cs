﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class CompanyResponse
    {
        public string Name { get; set; }
        public string EegCertificate { get; set; }
        public string RegCertificateIssueDate { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string District { get; set; }
        public string Inn { get; set; }

    }
}
