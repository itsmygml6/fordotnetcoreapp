﻿namespace Frontoffice.Repository.DTO
{
    public class CreateTransactionDataDTO
    {
        public string TransactionId { get; set; }
        public string ChequeId { get; set; }
    }
}

