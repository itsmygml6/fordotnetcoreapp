﻿namespace Frontoffice.Repository.DTO.Payment
{
    public class PaymeDTO
    {
        public string method { get; set; }
    }

    public class PaymeParams
    {
        public string id { get; set; }
        public long time { get; set; }
        public decimal amount { get; set; }
        public PaymeAccount account { get; set; }
    }

    public class PaymeAccount
    {
        public string phone { get; set; }
    }
}
