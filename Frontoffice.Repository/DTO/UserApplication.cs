﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.DTO
{
    public class UserApplication
    {
        public long Id { get; set; }
        public string Client { get; set; }
        public string Product { get; set; }
        public decimal InsuranceSum { get; set; }
        public decimal InsurancePremium { get; set; }
        public string State { get; set; }
        public long ProductTemplateId { get; set; }
        public long CounteragentId { get; set; }
    }
}
