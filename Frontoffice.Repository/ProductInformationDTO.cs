﻿namespace Frontoffice.Repository
{
    public class ProductInformationDTO
    {
        public string Title { get; set; } = String.Empty;
        public string Description { get; set; } = String.Empty;
        public string CreatedBy { get; set; } = String.Empty;
        public string CreatedDate { get; set; } = String.Empty;
    }
}
