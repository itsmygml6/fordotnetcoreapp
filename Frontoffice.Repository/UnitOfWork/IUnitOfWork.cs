﻿using CRM.Data.Models;
using CRM.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontoffice.Repository.UnitOfWork
{
    public interface IUnitOfWork
    {
        Task Save();
        IGenericRepository<SellerProduct> SellerProducts { get; }
        IGenericRepository<SellerCounteragent> SellerCounteragents { get; }
        IGenericRepository<InsuranceClass> InsuranceClasses { get; }
        IGenericRepository<ProductType> ProductTypes { get; }
        IGenericRepository<ProductTemplate> ProductTemplates { get; }
        IGenericRepository<Application> Applications { get; }
        IGenericRepository<RisksSection> Risks { get; }
        IGenericRepository<LimitSection> Limits { get; }
        IGenericRepository<RatesSection> Rates { get; }
        IGenericRepository<PlanningSection> Plannings { get; }
        IGenericRepository<ProductStage> ProductStages { get; }
        IGenericRepository<Attachment> Attachments { get; }
        IGenericRepository<Bill> Bills { get; }
        IGenericRepository<Contract> Contracts { get; }
        IGenericRepository<PaymentSourceType> Payments { get; }
        IGenericRepository<Policy> Policies { get; }
        IGenericRepository<Client> Clients { get; }
        IGenericRepository<Deal> Deals { get; }
    }
}
