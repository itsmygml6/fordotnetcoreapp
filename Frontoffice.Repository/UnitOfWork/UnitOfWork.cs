﻿using CRM.Data;
using CRM.Data.Models;
using CRM.Repository;

namespace Frontoffice.Repository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Private members
        private readonly AppDbContext _context;
        private IGenericRepository<ProductTemplate> _productTemplates;
        private IGenericRepository<ProductType> _productTypes;
        private IGenericRepository<InsuranceClass> _insuranceClasses;
        private IGenericRepository<Application> _applications;
        private IGenericRepository<RisksSection> _risks;
        private IGenericRepository<LimitSection> _limits;
        private IGenericRepository<RatesSection> _rates;
        private IGenericRepository<PlanningSection> _plannings;
        private IGenericRepository<ProductStage> _productStages;
        private IGenericRepository<Contract> _contracts;
        private IGenericRepository<PaymentSourceType> _payment;
        private IGenericRepository<Policy> _policies;
        private IGenericRepository<Client> _clients;
        private IGenericRepository<Deal> _deals;
        private IGenericRepository<SellerProduct> _sellerProducts;
        private IGenericRepository<SellerCounteragent> _sellerCounteragents;

        private IGenericRepository<Attachment> _attachments;

        private IGenericRepository<Bill> _bills;
        #endregion
        public UnitOfWork(AppDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        #region Public Members
        public IGenericRepository<ProductTemplate> ProductTemplates => _productTemplates ??= new GenericRepository<ProductTemplate>(_context);
        public IGenericRepository<ProductType> ProductTypes => _productTypes ??= new GenericRepository<ProductType>(_context);
        public IGenericRepository<InsuranceClass> InsuranceClasses => _insuranceClasses ??= new GenericRepository<InsuranceClass>(_context);
        public IGenericRepository<Application> Applications => _applications ??= new GenericRepository<Application>(_context);
        public IGenericRepository<RisksSection> Risks => _risks ??= new GenericRepository<RisksSection>(_context);
        public IGenericRepository<LimitSection> Limits => _limits ??= new GenericRepository<LimitSection>(_context);
        public IGenericRepository<RatesSection> Rates => _rates ??= new GenericRepository<RatesSection>(_context);
        public IGenericRepository<PlanningSection> Plannings => _plannings ??= new GenericRepository<PlanningSection>(_context);
        public IGenericRepository<ProductStage> ProductStages => _productStages ??= new GenericRepository<ProductStage>(_context);
        public IGenericRepository<Attachment> Attachments => _attachments ??= new GenericRepository<Attachment>(_context);
        public IGenericRepository<SellerProduct> SellerProducts => _sellerProducts ??= new GenericRepository<SellerProduct>(_context);
        public IGenericRepository<SellerCounteragent> SellerCounteragents => _sellerCounteragents ??= new GenericRepository<SellerCounteragent>(_context);

        public IGenericRepository<Bill> Bills => _bills ??= new GenericRepository<Bill>(_context);
        public IGenericRepository<Contract> Contracts => _contracts ??= new GenericRepository<Contract>(_context);
        public IGenericRepository<PaymentSourceType> Payments => _payment ??= new GenericRepository<PaymentSourceType>(_context);
        public IGenericRepository<Policy> Policies => _policies ??= new GenericRepository<Policy>(_context);
        public IGenericRepository<Client> Clients => _clients ??= new GenericRepository<Client>(_context);
        public IGenericRepository<Deal> Deals => _deals ??= new GenericRepository<Deal>(_context);

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }
        #endregion
    }
}
