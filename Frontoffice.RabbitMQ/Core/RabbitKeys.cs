﻿namespace Frontoffice.RabbitMQ.Core
{
    public struct RabbitKeys
    {
        public const string NEW_DOCUMENT_QUEUE = "NEW_DOCUMENT_QUEUE";
        public const string UPDATE_DOCUMENT_QUEUE = "UPDATE_DOCUMENT_QUEUE";
        public const string SIGNED_DOCUMENT_QUEUE = "SIGNED_DOCUMENT_QUEUE";
    }
}