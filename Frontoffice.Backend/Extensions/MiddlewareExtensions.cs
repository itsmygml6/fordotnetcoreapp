
using Frontoffice.Backend.Handlers;

namespace Frontoffice.Backend.Extensions
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseAppException(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
