﻿using CRM.Data;
using Microsoft.EntityFrameworkCore;

namespace Frontoffice.Backend.Extensions
{
    public class FunctionalExtensions
    {
        public static async Task<List<string>> GetSellerUsersByUserIdAsync(AppDbContext _context, string userId)
        {
            var user = await _context.Users.FirstOrDefaultAsync(p => p.Id.ToString() == userId);
            if (user is null)
            {
                throw new ArgumentNullException("User is not found");
            }

            return await _context.Users.Where(p => p.SellerId == user.SellerId).Select(p => p.Id.ToString()).ToListAsync();
        }
    }
}
