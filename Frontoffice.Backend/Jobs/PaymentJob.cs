﻿using CRM.Data.Core;
using CRM.Data.Enums;
using CRM.Data.Enums.Bills;
using Frontoffice.Backend.Services;
using Frontoffice.Backend.Services.Payment;

namespace Frontoffice.Backend.Jobs
{
    public class PaymentJob : BackgroundService
    {
        private readonly IServiceProvider _services;
        private IAppDbContext _db;
        private PaymentApiService _paymentService;
        private PolicyService _policyService;
        public PaymentJob(IServiceProvider services)
        {
            _services = services;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var scope = _services.CreateScope();
            _db = scope.ServiceProvider.GetService<IAppDbContext>();
            _paymentService = _services.GetService<PaymentApiService>();
            _policyService = _services.GetService<PolicyService>();

            await Task.Factory.StartNew(async () =>
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                      await CancelBills();
                        await UpdateToPaid();
                    

                    

                    await Task.Delay(2000, stoppingToken);
                }
            });
            
        }
        private async Task CancelBills()
        {
            try
            {
                var bills = _db.Bills.Where(x => x.CreatedDate.AddMinutes(120) < DateTime.Now && !string.IsNullOrEmpty(x.ReceiptId) && (x.Process == BillProcess.Process
                                            || x.Process == BillProcess.Paying)).ToList();

                if (bills.Any())
                {
                    foreach (var bill in bills)
                    {
                        //checkstatus
                        var result = await _paymentService.CancelReceipt(bill.ReceiptId);
                        if (result == ChequeState.Canceled)
                        {
                            bill.Process = BillProcess.Canceled;
                            bill.State = BillState.Canceled;
                            _db.Bills.Update(bill);
                        }

                    }

                    await _db.SaveChangesAsync();
                }

            }

            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
    
        }
        
        private async Task UpdateToPaid()
        {
            try 
            {
                var bills = _db.Bills.Where(x => x.Process == BillProcess.Paying).ToList();

                if (bills.Any())
                {
                    ///
                    foreach (var bill in bills)
                    {
                        //checkstatus
                        var result = await _paymentService.CheckReceipt(bill.ReceiptId);
                        if (result == ChequeState.Paid)
                        {
                            bill.Process = BillProcess.Success;
                            bill.State = BillState.Paid;
                            _db.Bills.Update(bill);


                            var policy = await _policyService.CreatePolicyAsync(_db, bill.ApplicationId.Value, bill.UserLogin);
                            await _policyService.PolicySend(policy.Id);
                        }

                        if (result == ChequeState.Canceled)
                        {
                            bill.Process = BillProcess.Canceled;
                            bill.State = BillState.Canceled;
                            _db.Bills.Update(bill);
                        }


                    }

                    await _db.SaveChangesAsync();
                }

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
            
        }
    }
}
