﻿using CRM.Data.Models;

namespace Frontoffice.Backend.Services
{
    public class ProductInformation
    {
        public static string[] GetInsuranceClasses(IEnumerable<ProductTemplateClass> insuranceClasses)
        {
            return insuranceClasses.Select(x => x.InsuranceClass.Title).ToArray();
        }

        public static string[] GetStages(IEnumerable<ProductStage> stages)
        {
            return stages.Select(x => x.StageType.ToString()).ToArray();
        }
    }
}
