﻿using CRM.Common;
using Newtonsoft.Json;

namespace Frontoffice.Backend.Services
{
    public class IdentityService
    {
        public static async Task<UserInfo?> GetUserInfoAsync(HttpClient _httpClient, string userId, AppConfig config)
        {
            var response = JsonConvert.DeserializeObject<ApiResponse>(await _httpClient.GetStringAsync($"{config.TokenUrl}/User/Get/{userId}"))!;
            
            return response.Success ? JsonConvert.DeserializeObject<UserInfo>(response.Data.ToString()!) : null;
        }
    }
}
