﻿using CRM.Common;
using Newtonsoft.Json;
using System.Text;

namespace Frontoffice.Backend.Services
{
    public class OsagoService
    {
        private readonly HttpClient _httpClient;
        private readonly AppConfig _config;
        private readonly CarService _carService;

        public OsagoService(HttpClient httpClient, AppConfig config, CarService carService)
        {
            _httpClient = httpClient;
            _config = config;
            _carService = carService;
        }

        public async Task<VehicleResponse?> GetVehicleAsync(VehicleDTO vehicleDTO)
        {
            var content = new StringContent(JsonConvert.SerializeObject(vehicleDTO), Encoding.UTF8,
                ApiResponseType.JsonResponse);
            var response = await _httpClient.PostAsync($"{_config.FoundIntegrationApi}info/vehicle", content);

            if (!response.IsSuccessStatusCode)
            {

                throw new InvalidOperationException(response.StatusCode.ToString());

            }

            var data = JsonConvert.DeserializeObject<ApiResponse>(await response.Content.ReadAsStringAsync());

            if (data.Success)
            {
                var tmp = JsonConvert.DeserializeObject<VehicleResponse>(data.Data.ToString());
                tmp.RegionId = _carService.GetRegionId(tmp.GovNumber);
                tmp.VehicleLicenceNumber = vehicleDTO.techPassportNumber;
                tmp.VehicleLicenceSeria = vehicleDTO.techPassportSeria;
                return tmp;
            }
            else
            {
                throw new InvalidOperationException(data.Error);
            }
        }

        public async Task<PersonResponse?> GetPersonAsync(PersonDTO personDTO)
        {
            var content = new StringContent(JsonConvert.SerializeObject(personDTO), Encoding.UTF8,
                ApiResponseType.JsonResponse);
            var response = await _httpClient.PostAsync($"{_config.FoundIntegrationApi}info/personbypassport", content);

            if (!response.IsSuccessStatusCode)
            {

                throw new InvalidOperationException(response.StatusCode.ToString());

            }

            var data = JsonConvert.DeserializeObject<ApiResponse>(await response.Content.ReadAsStringAsync());

            if (data.Success)
            {
                return JsonConvert.DeserializeObject<PersonResponse>(data.Data.ToString());
            }
            else
            {
                throw new InvalidOperationException(data.Error);
            }
        }

        public async Task<CompanyResponse?> GetCompanyAsync(CompanyDTO dto)
        {
            var content = new StringContent(JsonConvert.SerializeObject(dto), Encoding.UTF8,
                ApiResponseType.JsonResponse);
            var response = await _httpClient.PostAsync($"{_config.FoundIntegrationApi}info/company", content);

            if (!response.IsSuccessStatusCode)
            {

                throw new InvalidOperationException(response.StatusCode.ToString());

            }

            var data = JsonConvert.DeserializeObject<ApiResponse>(await response.Content.ReadAsStringAsync());

            if (data.Success)
            {
                var tmp = JsonConvert.DeserializeObject<CompanyResponse>(data.Data.ToString());
                tmp.Inn = dto.Inn;
                return tmp;
            }
            else
            {
                throw new InvalidOperationException(data.Error);
            }
        }
    }
}
