﻿
using CRM.Common.Actions;
using CRM.Data.Core;
using Microsoft.EntityFrameworkCore;

namespace Frontoffice.Backend.Services
{
    public class PolicyService:ApiClient
    {
        public PolicyService(string baseUrl) : base(baseUrl)
        {
        }

        public async Task<Policy> CreatePolicyAsync(IAppDbContext db, long applicationId, string username)
        {
            try
            {
                var application = await db.Applications
                    .Where(p => p.Id == applicationId)
                    .Include(x => x.Deal)
                    .FirstOrDefaultAsync();
                var deal = await db.Deals.FirstOrDefaultAsync(x => x.Id == application.Deal.Id);
                deal.Stages = CRM.Data.Enums.SalesDealStages.Policy;
                
                db.Deals.Update(deal);
                await db.SaveChangesAsync();

                var policy = new Policy()
                {
                    DealId = application.Deal?.Id,
                    StartDate = application.InsuranceStart,
                    EndDate = application.InsuranceEnd,
                    CreateDate = DateTime.Now,
                    TemplateUID = application.TemplateUID,
                    DocumentUID = application.DocumentUID,
                    State = CRM.Data.Enums.SalesPolicyState.Created,
                    UserLogin = username,
                    RegistrationSerial = "EATB",
                    RegistrationNumber = NumberGenerator(applicationId)
                };

                //var deal = application.Deal;
               
                await db.Policies.AddAsync(policy);
                await db.SaveChangesAsync();

                return policy;
            }
            catch (Exception ex) { throw; }
        }

        private string NumberGenerator(long id)
        {
            var scount = $"{id}".Length;
            var nill = "";
            for (var i = 0; i < (7 - scount); i++)
                nill += "0";

            return $"{nill}{id}";
        }

        public async Task<bool> PolicySend(long policyId)
        {
            var result = await PostSimple<ActionResult>($"/registration/policy?policyId={policyId}", null);
            if(result.Status == ActionStatus.Failed)
                throw new Exception(result.Error.Message);

            return result.Status == ActionStatus.Success;
        }
    }
}
