﻿using Constructor.Backend.Services;
using CRM.Constructor.DTO;
using CRM.Data.Enums;
using CRM.Data.Models;
using CRM.Repository;
using Microsoft.EntityFrameworkCore;

namespace Constructor.Backend.Services
{
    public static class LimitService
    {
        public static async Task<bool> CalculateLimits(LimitValidationDTO dto, IUnitOfWork unitOfWork)
        {
            var product = await unitOfWork.ProductTemplates.Get(p => p.Id == dto.Product, include: p => p
                .Include(p => p.Limit).ThenInclude(p => p.Limits).ThenInclude(p => p.SubLimits));
            
            decimal insuranceSum;
            decimal insurancePremium;

            #region Product limits calculation
            
            if (product is not null)
            {
                #region Limit sum calculation
                
                if (product.IsInsuranceSumFixed)
                {
                    if (dto.Limit.IsSumPercent)
                    {
                        insuranceSum = dto.Limit.LimitSumMax ??= 0 * product.TotalInsuranceSum.Value / 100;
                    }
                    else
                    {
                        insuranceSum = dto.Limit.LimitSum ??= 0;
                    }
                }
                else
                {
                    if (dto.Limit.SubLimits is not null && dto.Limit.SubLimits.Count > 0)
                    {
                        insuranceSum = CalculateLimit(dto.Limit.SubLimits.ToList(), LimitSumCalculationType.InsuranceSum);
                    }
                    else
                    {
                        insuranceSum = dto.Limit.LimitSum ??= 0;
                    }
                }
                
                #endregion

                #region Limit premium calculation

                if (product.IsInsurancePremiumFixed)
                {
                    if (dto.Limit.IsPremiumPercent)
                    {
                        insurancePremium = dto.Limit.LimitPremiumMax ??= 0 * product.TotalInsurancePremium.Value / 100;
                    }
                    else
                    {
                        insurancePremium = dto.Limit.LimitPremium ??= 0;
                    }
                }
                else
                {
                    if (dto.Limit.SubLimits is not null && dto.Limit.SubLimits.Count > 0)
                    {
                        insurancePremium = CalculateLimit(dto.Limit.SubLimits.ToList(), LimitSumCalculationType.InsurancePremium);
                    }
                    else
                    {
                        insurancePremium = dto.Limit.LimitPremium ??= 0;
                    }
                }

                #endregion

                #region Exceptions
                
                if (insuranceSum > product.TotalInsuranceSum)
                {
                    return false;
                }
                if (insurancePremium > product.TotalInsurancePremium)
                {
                    return false;
                }
                
                #endregion
            }
            
            #endregion

            return true;
        }

        private static decimal CalculateLimit(List<LimitDTO> dtos, LimitSumCalculationType calculationType, int subLimitsCount = 0, decimal average = 0)
        {
            decimal limit = 0;
            switch (calculationType)
            {
                case LimitSumCalculationType.InsuranceSum:
                    limit = dtos[subLimitsCount].LimitSum ??= 0;
                    break;
                case LimitSumCalculationType.InsurancePremium:
                    limit = dtos[subLimitsCount].LimitPremium ??= 0;
                    break;
                case LimitSumCalculationType.InsuranceDeductible:
                    limit = dtos[subLimitsCount].DeductibleMax ??= 0;
                    break;
            }

            if (subLimitsCount == dtos.Count - 1) return average + limit;

            return CalculateLimit(dtos, calculationType, ++subLimitsCount, average + limit);
        }
    }
    
    internal enum LimitSumCalculationType
    {
        InsuranceSum = 0,
        InsurancePremium,
        InsuranceDeductible
    }
}
