﻿using System.Text.RegularExpressions;

namespace Frontoffice.Backend.Services
{
    public class CarService
    {
        public int? GetRegionId(string carNumber) 
        {
            string ext = carNumber.Substring(0, 2);
            return Regex.IsMatch(ext, "^0[0-9]") ? 10 :
                    Regex.IsMatch(ext, "^1[0-9]") ? 11 :
                    Regex.IsMatch(ext, "^2[0-4]") ? 12 :
                    Regex.IsMatch(ext, "^2[5-9]") ? 13 :
                    Regex.IsMatch(ext, "^3[0-9]") ? 14 :
                    Regex.IsMatch(ext, "^4[0-9]") ? 15 :
                    Regex.IsMatch(ext, "^5[0-9]") ? 16 :
                    Regex.IsMatch(ext, "^6[0-9]") ? 17 :
                    Regex.IsMatch(ext, "^7[0-4]") ? 18 :
                    Regex.IsMatch(ext, "^7[5-9]") ? 19 :
                    Regex.IsMatch(ext, "^8[0-4]") ? 20 :
                    Regex.IsMatch(ext, "^8[5-9]") ? 21 :
                    Regex.IsMatch(ext, "^9[0-4]") ? 22 :
                    Regex.IsMatch(ext, "^9[5-9]") ? 23 : null;
        }
    }
}
