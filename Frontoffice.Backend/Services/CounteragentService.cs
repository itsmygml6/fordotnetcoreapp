﻿using CRM.Common;
using Frontoffice.Backend.Consts;
using Newtonsoft.Json;

namespace Frontoffice.Backend.Services
{
    public class CounteragentService
    {       

        public static async Task<Select?>? GetCounteragentByIdAsync(HttpClient _httpClient, long? counteragentId, AppConfig appConfig)
        {
            try
            {
                var response = await _httpClient.GetAsync($"{appConfig.BaseUrlCounterAgent}rest/getcounteragent/{counteragentId}");

                return response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<Select>(await response.Content.ReadAsStringAsync()) : null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
