﻿using CRM.Data.Core;
using Microsoft.EntityFrameworkCore;

namespace Frontoffice.Backend.Services
{
    public class UserService
    {
        private readonly IAppDbContext _db;

        public UserService(IAppDbContext db)
        {
            _db = db;
        }

        public async Task<User> GetUser(string userId) 
        {
            var user = await _db.Users.FirstOrDefaultAsync(x => x.Id.ToString() == userId);

            return user;
        }

    }
}
