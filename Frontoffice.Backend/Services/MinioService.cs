﻿using CRM.Data.Models;
using Minio;
using Minio.DataModel;

namespace Frontoffice.Backend.Services
{
    /// <summary>
    /// Class with static methods to upload and download files from MinIO server. All methods are async.
    /// </summary>
    public class MinioService
    {
        private static readonly string[] GrantedFileTypes =
        {
            ".pdf", ".docx", ".ftlh", ".html", ".jpg", 
            ".jfif", ".jpe", ".jpeg", ".png"
        };
        private readonly MinioClient _minio;

        public MinioService(MinioClient minio)
        {
            _minio = minio;
        }

        /// <summary>
        /// Method uploads data to MinIO server.
        /// </summary>
        /// <param name="bucketName">Name of the directory in MinIO, in what to save file.</param>
        /// <param name="folderName">Folder name in what this file will be saved.</param>
        /// <param name="fileName">Original file name with extension.</param>
        /// <param name="stream">Just a stream.</param>
        /// <returns>Returns new file name that is GUID.</returns>
        /// <exception cref="Exception">Not supported file type. Supported: .pdf, .html, .docx, .ftlh</exception>
        public async Task<string> Upload(string bucketName, string folderName, string fileName, Stream stream)
        {
            string fileExtension = fileName.Substring(fileName.LastIndexOf('.'));
            if (!GrantedFileTypes.Contains(fileExtension))
            {
                throw new Exception("Not supported file type.");
            }

            string fileNameGuid = Guid.NewGuid().ToString();
            string newFileName = folderName + '/' + fileNameGuid + fileExtension;

            await _minio.PutObjectAsync(bucketName, newFileName, stream, stream.Length);

            return fileNameGuid;
        }
        
        /// <summary>
        /// Method uploads data to MinIO server.
        /// </summary>
        /// <param name="bucketName">Name of the directory in MinIO, in what to save file.</param>
        /// <param name="folderName">Folder name in what this file will be saved.</param>
        /// <param name="file">IFormFile to save it in MinIO and database.</param>
        /// <returns>Returns new file name that is GUID.</returns>
        /// <exception cref="Exception">Not supported file type. Supported: .pdf, .doc, .docx, .ftlh</exception>
        public async Task<string> Upload(string bucketName, string folderName, IFormFile file)
        {
            string fileName = file.FileName;
            
            string fileExtension = fileName.Substring(fileName.LastIndexOf('.'));
            if (!GrantedFileTypes.Contains(fileExtension))
            {
                throw new Exception("Not supported file type.");
            }

            string fileNameGuid = Guid.NewGuid().ToString();
            string newFileName = folderName + '/' + fileNameGuid + fileExtension;

            Stream fileStream = file.OpenReadStream();

            await _minio.PutObjectAsync(bucketName, newFileName, fileStream, fileStream.Length);

            return fileNameGuid;
        }

        /// <summary>
        /// Method downloads file stream from MinIO server.
        /// </summary>
        /// <param name="bucketName">Name of the directory in MinIO, in what to save file.</param>
        /// <param name="folderName">Folder name in what this file will be saved.</param>
        /// <param name="fileNameGuid">File name at MinIO server, that is GUID.</param>
        /// <returns>File stream.</returns>
        /// <exception cref="Exception">If there is no file with given name.</exception>
        public async Task<Stream> Download(string bucketName, string folderName, string fileNameGuid)
        {
            Stream fileData = new MemoryStream();
            fileData.Position = 0;

            await _minio.GetObjectAsync(bucketName, folderName + '/' + fileNameGuid, 
                stream => stream.CopyTo(fileData));

            if (fileData == null)
            {
                throw new Exception("Incorrect file name.");
            }

            return fileData;
        }

        /// <summary>
        /// Method returns meta of the file in MinIO server.
        /// </summary>
        /// <param name="bucketName">Name of the directory in MinIO, in what to save file.</param>
        /// <param name="folderName">Folder name in what this file will be saved.</param>
        /// <param name="fileNameGuid">File name at MinIO server, that is GUID.</param>
        /// <returns>ObjectStat object with file meta.</returns>
        public async Task<ObjectStat> GetStats(string bucketName, string folderName, string fileNameGuid)
        {
            return await _minio.StatObjectAsync(bucketName, folderName + '/' + fileNameGuid);
        }
        
        /// <summary>
        /// Method deletes file from MinIO server.
        /// </summary>
        /// <param name="bucketName">Name of the directory in MinIO, in what to save file.</param>
        /// <param name="folderName">Folder name in what this file will be saved.</param>
        /// <param name="fileNameGuid">File name at MinIO server, that is GUID.</param>
        public async Task Delete(string bucketName, string folderName, string fileNameGuid)
        {
            await _minio.RemoveObjectAsync(bucketName, folderName + '/' + fileNameGuid);
        }

        /// <summary>
        /// Method deletes group of files from MinIO server.
        /// </summary>
        /// <param name="bucketName">Name of the directory in MinIO, in what to save file.</param>
        /// <param name="fileNames">List of file names represent by GUID path. Example: "documents/79e05c3d-3b11-44d4-88a0-9f4fbd67bf4c.pdf"</param>
        public async Task Delete(string bucketName, IEnumerable<string> fileNames)
        {
            await _minio.RemoveObjectAsync(bucketName, fileNames);
        }
    }
}
