﻿namespace Frontoffice.Backend.Services
{
    public static class ApplicationCalculation
    {
        public static ApplicationCalculationResult Calculate(Application application)
        {
            decimal totalSum = 0;
            decimal totalPremium = 0;

            decimal? insuranceSum = 0;
            decimal? premiumMultiply = 1;
            
            foreach (var limit in application.Limits)
            {
                if (limit.IsSubLimit)
                    continue;
                if (limit.IsLimitPremium)
                {
                    premiumMultiply *= limit.LimitPremium;
                }

                if (limit.IsLimitSum)
                {
                    insuranceSum += limit.LimitSum;
                }
            }

            foreach (var rate in application.Rates)
            {
                premiumMultiply *= rate.Premium;
            }
            
            if (insuranceSum == null || insuranceSum.Value == 0)
            {
                return new ApplicationCalculationResult
                {
                    success = false,
                    info = "No sum limits are existed",
                    premium = 0,
                    sum = 0
                };
            }

            totalSum += insuranceSum.Value * (application.InsuranceSumRate != null ? application.InsuranceSumRate.Premium : 1);

            totalPremium += premiumMultiply.Value * (application.InsurancePremiumRate != null ? application.InsurancePremiumRate.Premium : 1);
            
            return new ApplicationCalculationResult
            {
                success = true,
                info = "ok",
                premium = totalPremium,
                sum = totalSum
            };
        }
    }
}
