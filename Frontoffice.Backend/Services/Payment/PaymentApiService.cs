﻿using CRM.Data.Enums;
using Frontoffice.Backend.Services.Payment.DTO;

namespace Frontoffice.Backend.Services.Payment
{
    public class PaymentApiService : ApiClient
    {
        public PaymentApiService(string baseUrl) : base(baseUrl)
        {
        }

        public async Task<CreateReceiptDto> CreateReceipt(dynamic objects)
        {
            var result = await Post<CreateReceiptDto>("create", objects); 
            return result;
        }

        public async Task<ChequeState> CheckReceipt(string receiptId)
        {
            var result = await Post<ChequeState>($"check?receiptId={receiptId}", null);
            return result;
        }
        
        public async Task<ChequeState> CancelReceipt(string receiptId)
        {
            var result = await Post<ChequeState>($"cancel?checkId={receiptId}", null);
            return result;
        }
    }
}
