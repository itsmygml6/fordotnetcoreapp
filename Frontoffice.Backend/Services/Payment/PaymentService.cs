﻿using System.Text;
using CRM.Common;
using CRM.Common.Actions.Models;
using CRM.Data.Enums;
using Frontoffice.Backend.Consts;
using Frontoffice.Repository.DTO.Billing;
using Microsoft.EntityFrameworkCore;
using CRM.Common.Actions.Enums;
using Frontoffice.Backend.Services.Payment;
using Frontoffice.Backend.Services.Payment.DTO;
using CRM.Data.Enums.Bills;

namespace Frontoffice.Backend.Services
{
    public class PaymentService
    {
        private static readonly string BasePaymeUrl = "https://checkout.paycom.uz/";
        private static readonly string ReturnUrl = "https://frontoffice.aic.uz/";
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly HttpClient _httpClient;
        private readonly AppConfig _appConfig;
        private readonly PaymentApiService paymentApiService;
        private decimal ChargedSum(decimal sum) => 100 * sum;
        public PaymentService(IMapper mapper, IUnitOfWork unitOfWork, HttpClient httpClient, AppConfig appConfig, PaymentApiService paymentApiService)
        {
            _unitOfWork = unitOfWork;
            _httpClient = httpClient;
            _mapper = mapper;
            _httpClient.BaseAddress = new Uri(appConfig.BaseExternalPaymentApi);
            _appConfig = appConfig;
            this.paymentApiService = paymentApiService;
        }
        
        public async Task<CreateReceiptDto> CreateReceipt(Bill bill)
        {
            //var response = await _httpClient.PostAsJsonAsync("create", new { Amount = bill.ChargedSum*100, PhoneNumber = bill.Number });

            var result = await paymentApiService.CreateReceipt(new { ChequeId = bill.Id, Amount = bill.ChargedSum * 100, PhoneNumber = bill.Number });

            if (result == null)
            {
                throw new Exception("Receipt creation unknown");
            }
            bill.ChequeState = ChequeState.CreatingTransaction;
            
            bill.ReceiptId = result.ReceiptId;
            _unitOfWork.Bills.Update(bill);
            await _unitOfWork.Save();
            return result;
        }

        public async Task<string> SendReceipt(string receiptId, string phoneNumber)
        {
            return await (await _httpClient.PostAsJsonAsync("send/", new { CheckId = receiptId, PhoneNumber = phoneNumber })).Content.ReadAsStringAsync();
        }
        
        public async Task<string> CreatePayment(long billId)
        {
            var bill = await _unitOfWork.Bills.Get(x => x.Id == billId);
            if (bill.Process == BillProcess.Paying || bill.Process == BillProcess.Success)
                throw new Exception("Bill is paid");
            if (bill == null)
                throw new Exception("Bill not found");

            var receiptId = (await CreateReceipt(bill)).ReceiptId;
            
            var paymentUrl = new StringBuilder("m=" + _appConfig.ApexMerchantId + ";");
            paymentUrl.Append($"ac.cheque={receiptId};");
            paymentUrl.Append($"ac.phone={bill.Number};");
            paymentUrl.Append($"a={bill.ChargedSum * 100};");
            paymentUrl.Append("l=ru;");
            paymentUrl.Append($"c={ReturnUrl};");
            paymentUrl.Append("ct=1000;");
            paymentUrl.Append("cr=uzs");
            
            return BasePaymeUrl + Convert.ToBase64String(Encoding.UTF8.GetBytes(paymentUrl.ToString()));
        }

        public async Task<PaymentResponse<BillCreation>> GetBill(string receiptId)
        {
            var bill = await _unitOfWork.Bills.Get(p => p.ReceiptId == receiptId);
            if (bill is null)
            {
                if (bill == null)
                {
                    return new PaymentResponse<BillCreation>
                    {
                        PaymentObject = null,
                        Type = PaymentResponseType.Failed,
                        Reason = "Bill not found"

                    };
                }

            }
            var resultBill = _mapper.Map<BillCreation>(bill);
            return new PaymentResponse<BillCreation>
            {
                PaymentObject = resultBill,
                Type = PaymentResponseType.Success
            };
        }

        public async Task<string> Pay(long billId, decimal chargedSum, string phoneNumber, string url)
        {
            var bill = await _unitOfWork.Bills.Get(p => p.Id == billId, tracking: true);
            if (string.IsNullOrEmpty(url))
                url = ReturnUrl;

            //var createResult = await paymentApiService.CreateReceipt(new { Amount = ChargedSum(chargedSum), PhoneNumber = phoneNumber });
            ////var response = await _httpClient.PostAsJsonAsync("create/", new { Amount = chargedSum*100, PhoneNumber = phoneNumber });
            ////var checkId = (await response.Content.ReadAsStringAsync()).Trim('\"');
            //var cheque = Guid.NewGuid().ToString();
            //bill.ReceiptId = createResult.ReceiptId;
            bill.ChequeState = ChequeState.WaitingConfirmation;
            bill.ChargedSum = chargedSum;
            bill.Number = phoneNumber;
            bill.Process = BillProcess.Process;
            var paymentUrl = new StringBuilder("m=" + _appConfig.ApexMerchantId + ";");
            paymentUrl.Append($"ac.cheque={billId};");
            paymentUrl.Append($"ac.phone={bill.Number};");
            paymentUrl.Append($"a={ChargedSum(chargedSum)};");
            paymentUrl.Append("l=ru;");
            paymentUrl.Append($"c={url};");
            paymentUrl.Append("ct=1000;");
            paymentUrl.Append("cr=uzs");

            _unitOfWork.Bills.Update(bill);
            await _unitOfWork.Save();
            return BasePaymeUrl + Convert.ToBase64String(Encoding.UTF8.GetBytes(paymentUrl.ToString()));
        }
        
        public async Task<PaymentResponse<CreateTransactionPaymentObject>> CreateTransaction(long billId, string receiptId, long time)
        {
            var bill = await _unitOfWork.Bills.Get(p => p.Id == billId, tracking: true);
            if (bill is null)
            {
                throw new Exception("Bill not found");
            }
            
            bill.ReceiptId = receiptId;
            bill.TransactionId = await GetTransactionId(receiptId, time);
            bill.CreateTransactionTime = time;
            bill.State = BillState.Charged;
            bill.Process =  BillProcess.Process;
            bill.ChequeState = ChequeState.CreatingTransaction;
           
            await _unitOfWork.Save();
            
            return new PaymentResponse<CreateTransactionPaymentObject>
            {
                Type = PaymentResponseType.Success,
                PaymentObject = new CreateTransactionPaymentObject
                {
                    TransactionId = bill.TransactionId,
                }
            };
        }

        public async Task<PaymentResponse> PerformTransaction(string receiptId)
        {
            var bill = await _unitOfWork.Bills.Get(p => p.ReceiptId == receiptId, tracking: true);

            if (bill == null)
            {
                return new PaymentResponse
                {
                    Type = PaymentResponseType.Failed,
                    Reason = "Bill not found"
                
                };
            }

            if (bill.State == BillState.Paid)
            {
                return new PaymentResponse
                {
                    Type = PaymentResponseType.Cancelled,
                    Reason = "Bill status is paid"

                };
            }
            bill.Process = BillProcess.Paying;
            bill.PerformTransactionTime = DatetimeToTimestampConvert(DateTime.Now);
            bill.PayedSum = bill.ChargedSum;
            
            
            //bill.State = BillState.Charged;

            //var application = await _unitOfWork.Applications.Get(p => p.PaymentBillId == bill.Id, 
            //    include: p => p.Include(p => p.Deal));
            //try
            //{
            //    application.Deal.Stages = SalesDealStages.Policy;
            //}
            //catch {}
            
            _unitOfWork.Bills.Update(bill);
            await _unitOfWork.Save();
            
            return new PaymentResponse
            {
                Type = PaymentResponseType.Success
            };

        }

        public async Task<PaymentResponse> CancelTransaction(string transactionId, TransactionResult reason)
        {
            var bill = await _unitOfWork.Bills.Get(p => p.TransactionId == transactionId, tracking: true);
            if (bill == null)
            {
                return new PaymentResponse
                {
                    Type = PaymentResponseType.Failed,
                    Reason = "Bill not found"

                };
            }
                        
            bill.CancelTransactionTime = DatetimeToTimestampConvert(DateTime.Now);
            bill.State = BillState.Canceled;
            bill.Process = BillProcess.Canceled;
            bill.TransactionResult = reason;

            _unitOfWork.Bills.Update(bill);
            await _unitOfWork.Save();

            return new PaymentResponse
            {
                Type = PaymentResponseType.Cancelled
            };
        }
        
        public async Task<string> GenerateTransactionId()
        {
            var bills = (await _unitOfWork.Bills.GetAll()).ToList();
            return bills.Count + 1 + "";
        }
        
        public async Task<string> GetTransactionId(string receiptId, long time)
        {
            return receiptId+time;
        }
        
        public static long DatetimeToTimestampConvert(DateTime date)
        {
            long unixTimestamp = (long)(date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;

            return unixTimestamp;
        }
    }
}
