﻿namespace Frontoffice.Backend.Services.Payment.DTO
{
    public class CheckReceiptDto
    {
        public string ReceiptId { get; set; }
    }
}
