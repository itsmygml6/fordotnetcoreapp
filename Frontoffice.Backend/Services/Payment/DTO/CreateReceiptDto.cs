﻿namespace Frontoffice.Backend.Services.Payment.DTO
{
    public class CreateReceiptDto
    {
        
        public string ReceiptId { get; set; }
        public string Cheque { get; set;}
    }
}
