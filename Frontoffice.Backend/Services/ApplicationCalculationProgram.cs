﻿using CRM.Data.Models;


namespace Frontoffice.Backend.Services
{
    public static class ApplicationCalculationProgram
    {
        public static ApplicationCalculationResult Calculate(Application application)
        {
            decimal totalSum = 0;
            decimal totalPremium = 0;

            if (application.Programs == null)
            {
                return new ApplicationCalculationResult { 
                    success = false, 
                    info = "No available programs are existed", 
                    premium = 0, 
                    sum = 0 };
            }

            foreach(var programm in application.Programs)
            {

                decimal? insuranceSum = 0;
                decimal? premiumMultiply = 1;
                
                foreach (var limit in programm.Limits)
                {
                    if (limit == null)
                        continue;
                    if (limit.IsSubLimit)
                        continue;
                    if (limit.IsLimitPremium)
                    {
                        premiumMultiply *= limit.LimitPremium;
                    }

                    if (limit.IsLimitSum)
                    {
                        insuranceSum += limit.LimitSum;
                    }

                }

                foreach (var rate in programm.Rates)
                {
                    if (rate == null) 
                        continue;

                    premiumMultiply *= rate.Premium;
                }
                
                if (insuranceSum == null || insuranceSum.Value == 0)
                {
                    return new ApplicationCalculationResult
                    {
                        success = false,
                        info = "No sum limits are existed",
                        premium = 0,
                        sum = 0
                    };
                }

                totalSum += insuranceSum.Value * (application.InsuranceSumRate != null ? application.InsuranceSumRate.Premium : 1);

                totalPremium += premiumMultiply.Value * (application.InsurancePremiumRate != null ? application.InsurancePremiumRate.Premium : 1);
            }
            return new ApplicationCalculationResult
            {
                success = true,
                info = "ok",
                premium = totalPremium,
                sum = totalSum
            };
        }
    }
}
