﻿using CRM.Common;
using CRM.Data;
using CRM.Data.Core;
using Frontoffice.Backend.Extensions;
using Frontoffice.Backend.Jobs;
using Frontoffice.Backend.Services;
using Frontoffice.Backend.Services.Payment;
using Frontoffice.Repository.UnitOfWork;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Minio;
using Minio.AspNetCore;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Frontoffice.Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            HostingEnvironment = environment;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment HostingEnvironment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var config = new AppConfig();
            Configuration.GetSection("Config").Bind(config);
            services.AddSingleton(config);
            services.AddControllersWithViews()
                .AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            );
            services.AddHttpContextAccessor();
            services.AddControllers();
            
            services.AddDbContext<IAppDbContext, AppDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<PaymentService>();
            services.AddTransient(x=> new PaymentApiService(config.BaseExternalPaymentApi));
            services.AddTransient<UserService>();
            services.AddTransient<CarService>();
            services.AddTransient<OsagoService>();
            services.AddTransient(x=>new PolicyService(config.BaseDocumentRegistrationUrl));
            services.AddHostedService<PaymentJob>();
            
            services.AddMinio(new Uri(Configuration.GetConnectionString("MinIO")));
            services.AddSingleton<MinioService>();
            
            services.AddAutoMapper(typeof(Startup));
            
            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
                {
                    options.MetadataAddress = $"{config.AuthUrl}/auth/realms/{config.Realm}/.well-known/openid-configuration";
                    options.RequireHttpsMetadata = false;
                    options.IncludeErrorDetails = true;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateLifetime = true,
                        ValidateAudience = false,
                        ValidateIssuerSigningKey = true,
                        ValidateTokenReplay = true,
                        ValidateActor = false,
                        ValidateIssuer = true,
                    };
                });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = $"{config.Title} API",
                    Version = $"v1"
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                    new string[] {}
                    }
                });

            });

            services.AddHttpClient();

            services.AddAuthorization();
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", builder => builder.SetIsOriginAllowed(a => true).AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            });


            services.AddHealthChecks()
                .AddNpgSql(Configuration.GetConnectionString("DefaultConnection"));

        }

        public void Configure(IApplicationBuilder app, AppConfig config)
        {
            app.UseAppException();

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);

            app.UseRouting();

            app.UseCors("AllowAll");
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("health", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });

                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.DisplayRequestDuration();
                c.DocExpansion(DocExpansion.None);
                c.ShowExtensions();
                c.EnableValidator();

                c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{config.Title} API V1");
            });
        }
    }
}
