using AutoMapper;
using CRM.Common.Extensions;
using CRM.Common.Rest;
using CRM.Constructor.DTO;
using CRM.Data;
using CRM.Data.Enums.Bills;
using Frontoffice.Backend.Extensions;
using Frontoffice.Repository.DTO.Billing;
using Frontoffice.Repository.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    public class BillController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContext;
        private readonly AppDbContext _context;

        public BillController(IUnitOfWork unitOfWork, IMapper mapper, IHttpContextAccessor httpContext, AppDbContext context)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        [HttpPost]
        public async Task<IActionResult> Create(BillCreation billDto)
        {
            try
            {
                billDto = billDto.IsNotScript();
            }
            catch
            {
                return ValidationProblem("Model is not valid");
            }

            var bill = _mapper.Map<Bill>(billDto);
            await _unitOfWork.Bills.Insert(bill);
            await _unitOfWork.Save();
            return Ok(bill);
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(long Id)
        {
            var bill = await _unitOfWork.Bills.Get(P => P.Id == Id);
            if (bill == null)
            {
                return NotFound("Bill is not found");
            }
            var res = _mapper.Map<BillDTO>(bill);

            res.Status = bill.Process.ToString();

            return Ok(res);
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> Delete(long Id)
        {
            var bill = await _unitOfWork.Bills.Get(P => P.Id == Id);

            if (bill == null)
            {
                return NotFound("Bill is not found");
            }

            bill.IsDeleted = true;
            _unitOfWork.Bills.Update(bill);
            await _unitOfWork.Save();
            return NoContent();
        }
        // UI ga qarab DTO yasash kerak
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] RequestParams requestParams)
        {
            var userId = _httpContext?.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (string.IsNullOrWhiteSpace(userId))
            {
                return NotFound("User not found");
            }
            try
            {
                var users = await FunctionalExtensions.GetSellerUsersByUserIdAsync(_context, userId);
                var bills = await _unitOfWork.Bills.GetPagedList(requestParams, p => users.Contains(p.CreatedBy), include: p => p.Include(t => t.Application).ThenInclude(t => t.Deal), orderBy: p => p.OrderByDescending(t => t.Id));

                var response = _mapper.Map<ResponseDTO>(bills);

                var data = bills.Select(p => new BillsDTO()
                {
                    Id = p.Id,
                    BillNo = p.Number,
                    DealNo = p.Application?.Deal?.Number,
                    ChargedDate = p.CreatedDate,
                    ChargedPremium = p.ChargedSum,
                    DueDate = p.DueDate,
                    PaidDate = p.PayedDate,
                    PaidPremium = p.PayedSum,
                    State = p.State.ToString(),
                    Status = p.Process.ToString(),
                });

                response.Data = data;

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> Update(long Id, BillCreation dto)
        {
            try
            {
                dto = dto.IsNotScript();
            }
            catch
            {
                return ValidationProblem("Model is not valid");
            }
            var bill = await _unitOfWork.Bills.Get(P => P.Id == Id);
            if (bill == null)
            {
                return NotFound("Bill is not found");
            }

            _mapper.Map(dto, bill);
            bill.Id = Id;
            _unitOfWork.Bills.Update(bill);
            await _unitOfWork.Save();
            return Ok(bill);
        }

    }
}
