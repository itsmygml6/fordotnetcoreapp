﻿using CRM.Data;
using CRM.Data.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Frontoffice.Backend.Services;
using CRM.Data.Enums.Bills;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    [Authorize]
    public class ApplicationController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly AppDbContext _context;
        private readonly IHttpContextAccessor _httpContext;
        public ApplicationController(IUnitOfWork unitOfWork, IMapper mapper, AppDbContext context, IHttpContextAccessor httpContext)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
        }

        [HttpPost]
        public async Task<ActionResult> Create(ApplicationIncomingDTO applicationDto)
        {
            var app = _mapper.Map<Application>(applicationDto);

            var userName = _httpContext?.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (string.IsNullOrWhiteSpace(userName))
                userName = Environment.UserName ?? "(local)";

            app.Deal = null;

            await _unitOfWork.Applications.Insert(app);
            await _unitOfWork.Save();

            var counteragent = applicationDto.Participants?.FirstOrDefault(p => p.Role == RoleType.Insurant);

            var deal = new Deal()
            {
                InsurantCounteragentId = counteragent?.CounteragentId,
                ApplicationId = app.Id,
                Number = (long.Parse(await _context.Deals.MaxAsync(p => p.Number) ?? "100000") + 1).ToString(),
                SellerId = (await _context.Users.FirstOrDefaultAsync(p => p.Id.ToString() == userName))?.SellerId,
                SellerEmployeeId = userName,
                
            };

            var bill = new Bill()
            {
                IssuedDate = DateTime.Now,
                Number = "998909620764",
                ChargedSum = app.InsurancePremium,
                DueDate = DateTime.Now.AddHours(2),
                PayedSum = 0,
                PayedDate = DateTime.MinValue,
                State = BillState.Charged,
                AttachmentId = null,
                PaymentSourceTypeId = null,
                TransactionId = String.Empty,
                TransactionResult = 0,
                ReceiptId = String.Empty,
                CreateTransactionTime = 0,
                PerformTransactionTime = 0,
                CancelTransactionTime = 0,
                ApplicationId = app?.Id,
                UserLogin = userName

            };
            var counteragentId = counteragent?.CounteragentId;

            if (counteragentId.HasValue)
            {
                var client = await _unitOfWork.Clients.Get(x => x. CounteragentId == counteragentId.Value);
                
                if (client == null)
                {
                    var newclient = new Client
                    {
                        AccountManagerId = deal.SellerId,
                        CounteragentId = counteragentId.Value,
                        Type = ClientType.Retail,
                        State = ClientState.Active,
                        UserLogin = userName
                    };
                    await _unitOfWork.Clients.Insert(newclient);
                }
            }
            

            await _unitOfWork.Bills.Insert(bill);
            await _unitOfWork.Deals.Insert(deal);
            await _unitOfWork.Save();
            return Ok(app);
        }

        [HttpGet("/{id}")]
        public async Task<ActionResult> Get(long id)
        {
            var app = await _unitOfWork.Applications.Get(p => p.Id == id, include: t => t
                .Include(p => p.Attachments)
                .Include(p => p.Limits).ThenInclude(p => p.SubLimits)
                .Include(p => p.Limits).ThenInclude(p => p.Rate)
                .Include(p => p.Rates)
                .Include(p => p.Objects).ThenInclude(p => p.ObjectDetails)
                .Include(p => p.Participants)
                .Include(p => p.Risks).ThenInclude(p => p.Exclusions)
                .Include(p => p.DocumentTemplates).ThenInclude(p => p.Files)
                .Include(p => p.InsuranceSchedule)
                .Include(p => p.Deal));
            if (app is null)
            {
                return BadRequest("Application not found");
            }

            return Ok(app);
        }
        
        [HttpPost("/calculate")]
        public ActionResult Calculate(ApplicationIncomingDTO dto)
        {
            var app = _mapper.Map<Application>(dto);

            if (app.InsurancePremiumRate == null)
            {
                return BadRequest("Rate cannot be null!");
            }
            if (app.InsuranceSumRate == null)
            {
                return BadRequest("Rate cannot be null!");
            }

            ApplicationCalculationResult response;
            try
            {
                response = ApplicationCalculation.Calculate(app);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(response);
        }
    }
}
