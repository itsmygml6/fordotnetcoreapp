﻿using System.Net;
using System.Security.Claims;
using AutoMapper;
using CRM.Common;
using CRM.Common.Rest;
using CRM.Constructor.DTO;
using CRM.Data;
using Frontoffice.Backend.Consts;
using Frontoffice.Backend.Services;
using Frontoffice.Repository.DTO;
using Frontoffice.Repository.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    [Authorize]
    public class ClientsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContext;
        private readonly HttpClient _httpClient;
        private readonly AppDbContext _context;
        private readonly AppConfig _appConfig;

        public ClientsController(IMapper mapper, IUnitOfWork unitOfWork, IHttpContextAccessor httpContext, HttpClient httpClient, AppDbContext context, AppConfig appConfig)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _appConfig = appConfig ?? throw new ArgumentNullException(nameof(appConfig));
        }

        [HttpGet("getall")]
        public async Task<ActionResult> GetClients([FromQuery] RequestParams requestParams)
        {
            var userId = _httpContext?.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (string.IsNullOrWhiteSpace(userId))
            {
                return NotFound("User not found");
            }

            var user = await _context.Users.FirstOrDefaultAsync(p => p.Id.ToString() == userId);

            if (user == null)
            {
                return NotFound("User not found");
            }

            var clients = await _unitOfWork.Clients.GetPagedList(requestParams, p => !p.IsDeleted && p.AccountManagerId == user.SellerId, include: p => p.Include(c => c.AccountManager));


            if (clients is null)
            {
                return NotFound("Clients is not found.");
            }
            
            var response = _mapper.Map<ResponseDTO>(clients);
            response.Data = clients.Select(p => new 
            { 
                Id = p.Id,  
                AccountManager = CounteragentService.GetCounteragentByIdAsync(_httpClient, p.AccountManager?.CounteragentId, _appConfig)?.Result?.Title, 
                CounteragentId = p.CounteragentId,
                CounteragentTitle = CounteragentService.GetCounteragentByIdAsync(_httpClient, p.CounteragentId, _appConfig)?.Result?.Title,
                Type = p.Type.ToString(),
                CreatedBy = p.UserLogin,
                CreatedDate = p.CreatedDate,
                State = p.State.ToString()
            });
            return Ok(response);
        }

        [HttpGet("client/{id}")]
        public async Task<ActionResult> GetClientById(long id)
        {
            Client? client = await _unitOfWork.Clients.Get(p => p.Id == id);
            _httpClient.BaseAddress = new Uri(_appConfig.BaseUrlCounterAgent + "rest/"); //new Uri(AppConst.BaseUrlCounterAgent);
            //var counteragent = await CounteragentService.GetCounteragentByIdAsync(_httpClient, 1, _appConfig);
            //client.CounteragentTitle = counteragent?.Title;
            if (client is null)
            {
                return NotFound("Client is not found");
            }
            return Ok(client);
        }

        [HttpGet("deals/{id}")]
        public async Task<ActionResult> GetClientDeals(long id)
        {
            var client = await _unitOfWork.Deals.Get(p => p.Id == id);
            if (client is null)
            {
                return NotFound("Client is nor found");
            }
            return Ok(client);
        }

        [HttpGet("applications/{id}")]
        public async Task<ActionResult> GetClientApplication(long id)
        {
            var client = await _unitOfWork.Applications.Get(p => p.Id == id);
            if (client is null)
            {
                return NotFound("Client is nor found");
            }
            return Ok(client);
        }

        [HttpGet("contracts/{id}")]
        public async Task<ActionResult> GetClientContract(long id)
        {
            var client = await _unitOfWork.Contracts.Get(p => p.Id == id);
            if (client is null)
            {
                return NotFound("Client is nor found");
            }
            return Ok(client);
        }

        [HttpGet("payments/{id}")]
        public async Task<ActionResult> GetClientPayment(long id)
        {
            var client = await _unitOfWork.Payments.Get(p => p.Id == id);
            if (client is null)
            {
                return NotFound("Client is nor found");
            }
            return Ok(client);
        }

        [HttpGet("policies/{id}")]
        public async Task<ActionResult> GetClientPolicies(long id)
        {
            var client = await _unitOfWork.Policies.Get(p => p.Id == id);
            if (client is null)
            {
                return NotFound("Client is nor found");
            }
            return Ok(client);
        }
    }
}
