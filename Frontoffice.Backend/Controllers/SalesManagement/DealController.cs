using CRM.Common;
using CRM.Constructor.DTO;
using CRM.Data;
using Frontoffice.Backend.Extensions;
using Frontoffice.Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    [Authorize]
    public class DealController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly HttpClient _httpClient;
        private readonly AppConfig _configuration;
        private readonly IHttpContextAccessor _httpContext;
        private readonly AppDbContext _context;

        public DealController(IMapper mapper, IUnitOfWork unitOfWork, HttpClient httpClient, AppConfig configuration, IHttpContextAccessor httpContext, AppDbContext context)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        [HttpGet("applications")]
        public async Task<ActionResult> GetApplications()
        {
            var apps = await _unitOfWork.Applications.GetAll();
            if (apps is null)
            {
                return NotFound("Applications not found.");
            }
            return Ok(apps);
        }

        [HttpGet("contracts")]
        public async Task<ActionResult> GetContracts()
        {
            var apps = await _unitOfWork.Contracts.GetAll();
            if (apps is null)
            {
                return NotFound("Contracts not found.");
            }
            return Ok(apps);
        }

        [HttpGet("payments")]
        public async Task<ActionResult> GetPayments()
        {
            var apps = await _unitOfWork.Payments.GetAll();
            if (apps is null)
            {
                return NotFound("Payments not found.");
            }
            return Ok(apps);
        }

        [HttpGet("policies")]
        public async Task<ActionResult> GetPolices()
        {
            var apps = await _unitOfWork.Policies.GetAll();
            if (apps is null)
            {
                return NotFound("Policies not found.");
            }
            return Ok(apps);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] RequestParams requstParams)
        {

            var userId = _httpContext?.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (string.IsNullOrWhiteSpace(userId))
            {
                return NotFound("User not found");
            }

            try
            {
                var users = await FunctionalExtensions.GetSellerUsersByUserIdAsync(_context, userId);
                var deals = await _unitOfWork.Deals.GetPagedList(requstParams, c => !c.IsDeleted 
                    && users.Contains(c.CreatedBy) && !c.Application.IsDeleted,
                    include: p => p.Include(p => p.Application));
                var response = _mapper.Map<ResponseDTO>(deals);

                var data = new List<DealTableDTO>();

                foreach (var deal in deals)
                {
                    var client = await CounteragentService.GetCounteragentByIdAsync(_httpClient, deal.InsurantCounteragentId, _configuration);
                    data.Add(new DealTableDTO
                    {
                        Id = deal.Id,
                        Number = deal.Number,
                        ProductName = (await _unitOfWork.ProductTemplates.Get(p => p.Id == deal.Application.ProductTemplateId))?.Title,
                        TotalProductSum = deal.Application.InsuranceSum.ToString(),
                        Client = client?.Title
                    });
                }

                response.Data = data;

                return Ok(response);
            }
            catch(Exception ex)
            {
                return NotFound(ex.Message);
            }

            
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(long Id)
        {
            var deal = await _unitOfWork.Deals.Get(c => c.Id == Id, include: p => p.Include(p => p.Application).Include(p => p.Policy).Include(p => p.Contract));
            if (deal == null)
                return NotFound("contact is not found");

            var sellerUser = await IdentityService.GetUserInfoAsync(_httpClient, deal.SellerEmployeeId, _configuration);
            var data = new DealDTO()
            {
                Id = Id,
                Stage = deal.Stages,
                InsurancePremium = deal.Application.InsurancePremium,
                InsuranceSum = deal.Application.InsuranceSum,
                ContractDurationStart = deal.Application.EffectStart,
                ContractDurationEnd = deal.Application.EffectEnd,
                InsuranceDurationStart = deal.Application.InsuranceStart,
                InsuranceDurationEnd = deal.Application.InsuranceEnd,
                //PolicyDate = deal.Policy.CreateDate,
                //PolicySerial = deal.Policy.RegistrationNumber,
                //PolicyNumber = deal.Policy.RegistrationNumber,
                //ContractNumber =deal.Contract.RegistrationNumber,
                //ContractDate = deal.Contract.RegistrationDate,
                InsuranceProduct = ((await _unitOfWork.ProductTemplates.Get(p => p.Id == deal.Application.ProductTemplateId))?.Title),
                ApplicationDate = deal.Application.CreatedDate,
                ApplicationNumber = "",
                Seller = (await CounteragentService.GetCounteragentByIdAsync(_httpClient, deal.InsurantCounteragentId, _configuration))?.Title,
                ClientName = sellerUser.FirstName + " " + sellerUser.LastName,
                ClientEmail = sellerUser.Email,
                ClientPhone = sellerUser.CellPhone
            };

            return Ok(data);
        }

        [HttpGet("deal/{Id}")]
        public async Task<IActionResult> GetDealById(long Id)
        {
            var deal = await _unitOfWork.Deals.Get(c => c.Id == Id, include: t => t.Include(p => p.Application).Include(p => p.Policy).Include(p => p.Contract));
            if (deal == null)
                return NotFound("contact is not found");

            return Ok(deal);
        }
        
        [HttpGet("Search/{Number}")]
        public async Task<IActionResult> Search(string Number)
        {
            if (!String.IsNullOrEmpty(Number))
            {
                var deals = await _unitOfWork.Deals.GetAll( c => !c.IsDeleted && c.Number.Contains(Number) && c.ContractId == null);

                if (deals == null)
                    return NotFound("Deals is not found");

                var data = new List<DealForCreateContractDTO>();

                foreach (var deal in deals)
                {
                    var sellerUser = await IdentityService.GetUserInfoAsync(_httpClient, deal.SellerEmployeeId, _configuration);
                    var counteragent = await CounteragentService.GetCounteragentByIdAsync(_httpClient, deal.InsurantCounteragentId, _configuration);

                    data.Add(new DealForCreateContractDTO
                    {
                        SellerEmployeeId = sellerUser?.Id,
                        SellerEpmloyeeName = sellerUser?.FirstName + " " + sellerUser?.LastName,
                        SellerCounteragentId = counteragent?.Id,
                        SellerCounteragentName = counteragent?.Title,
                        DealNumber = deal.Number
                    });
                }
                return Ok(data);
            }

            return NotFound("Deal is not found");
        }
    }
}