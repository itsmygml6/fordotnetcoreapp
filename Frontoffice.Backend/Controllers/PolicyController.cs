﻿global using Microsoft.AspNetCore.Authorization;
using CRM.Common;
using CRM.Constructor.DTO;
using CRM.Data;
using Frontoffice.Backend.Extensions;
using Frontoffice.Backend.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    public class PolicyController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContext;
        private readonly HttpClient _httpClient;
        private readonly AppDbContext _context;
        private readonly AppConfig _appConfig;

        public PolicyController(IUnitOfWork unitOfWork, IMapper mapper, IHttpContextAccessor httpContext, HttpClient httpClient, AppDbContext context, AppConfig appConfig)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _appConfig = appConfig ?? throw new ArgumentNullException(nameof(appConfig));
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] RequestParams requestParams)
        {
            var userId = _httpContext?.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (string.IsNullOrWhiteSpace(userId))
            {
                return NotFound("User not found");
            }
            try
            {
                var user = await _context.Users.FirstOrDefaultAsync(p => p.Id.ToString() == userId);

                if (user is null)
                {
                    return NotFound("User not found");
                }

                var policies = await _unitOfWork.Policies.GetPagedList(requestParams, p => !p.IsDeleted && p.Deal.SellerId == user.SellerId,
                orderBy: p => p.OrderByDescending(t => t.Id), include: p => p.Include(t => t.Deal).ThenInclude(t => t.Application));

                var response = _mapper.Map<ResponseDTO>(policies);
                
                response.Data = policies.Select(p => new
                {
                    Id = p.Id,
                    Client = (CounteragentService.GetCounteragentByIdAsync(_httpClient, p.Deal?.InsurantCounteragentId, _appConfig))?.Result?.Title,
                    Application = p.Deal?.Number,
                    Premium = p.Deal?.Application?.InsurancePremium,
                    Sum = p.Deal?.Application?.InsuranceSum,
                    Start = p.StartDate,
                    End = p.EndDate,
                    State = p.State.ToString()
                });
                
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(long Id)
        {
            var policy = await _unitOfWork.Policies.Get(p => p.Id == Id);

            return Ok(policy);
        }
    }
}
