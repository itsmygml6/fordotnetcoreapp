﻿using CRM.Data.Enums;
using Frontoffice.Backend.Consts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using X.PagedList;
using Newtonsoft.Json;
using CRM.Constructor.DTO;
using Frontoffice.Backend.Services;
using CRM.Data.Core;
using CRM.Common;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    [Authorize]
    public class ApplicationInformationController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAppDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContext;
        private readonly HttpClient _httpClient;
        private readonly AppConfig _appConfig;

        public ApplicationInformationController(IUnitOfWork unitOfWork, IMapper mapper, IHttpContextAccessor httpContext, HttpClient httpClient, IAppDbContext dbContext, AppConfig appConfig)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _dbContext = dbContext;
            _appConfig = appConfig;
        }

        [HttpGet("insuranceclass")]
        public async Task<IActionResult> InsuranceClassSelect()
        {
            var classes = await _unitOfWork.InsuranceClasses.GetAll(p => !p.IsDeleted);
            return Ok(_mapper.Map<IEnumerable<Select>>(classes));
        }

        [HttpGet("producttype")]
        public async Task<IActionResult> ProductTypeSelect()
        {
            var productTypes = await _unitOfWork.ProductTypes.GetAll(p => !p.IsDeleted);

            return Ok(_mapper.Map<IEnumerable<Select>>(productTypes));
        }

        [HttpGet("product")]
        public async Task<IActionResult> ProductSelect(long classId = 0, long typeId = 0)
        {
            var userId = _httpContext?.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (string.IsNullOrWhiteSpace(userId))
            {
                return NotFound("User not found");            
            }
            else
            {
                var user = _dbContext.Users.FirstOrDefault(u => u.Id.ToString() == userId);

                if (user == null)
                {
                    return NotFound("User not found");
                }

                var seller = await _dbContext.Sellers.Where(s => s.Id == user.SellerId).Include(s => s.Products).FirstOrDefaultAsync();

                if (seller == null)
                {
                    return NotFound("Seller not found");
                }

                var sellerProductIds = new List<long>();


                foreach (var sProduct in seller.Products)
                {
                    if (sProduct != null)
                    {
                        if (sProduct.ProductId != null && !sProduct.IsDeleted)
                        {
                            sellerProductIds.Add(sProduct.ProductId.Value);
                        }
                    }
                }

                IEnumerable<ProductTemplate> products = null;

                if (sellerProductIds.Count > 0)
                {
                    products = await _unitOfWork.ProductTemplates.GetAll(p => !p.IsDeleted && p.Status == Status.Active && sellerProductIds.Contains(p.Id), includes: new List<string>() { "Classes" });

                    if (classId != 0)
                    {
                        products = products.Where(p => p.Classes.Any(p => p.InsuranceClassId == classId));
                    }

                    if (typeId != 0)
                    {
                        products = products.Where(p => p.TypeId == typeId);
                    }
                }

                return Ok(_mapper.Map<IEnumerable<Select>>(products));
            }            
        }

        [HttpGet("productinformation/{productId}")]
        public async Task<IActionResult> GetProductInformation(long productId)
        {
            var product = await _unitOfWork.ProductTemplates.Get(p => p.Id == productId, 
                include: p => p.Include(p => p.ProductStages).Include(p => p.Type).Include(p => p.Classes).ThenInclude(p => p.InsuranceClass));

            if(product == null)
                return NotFound("Product is not found");

            return Ok(_mapper.Map<ProductResponse>(product));
        }

        [HttpGet("product/{productId}")]
        public async Task<IActionResult> GetProduct(long productId)
        {
            var userId = _httpContext?.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (string.IsNullOrWhiteSpace(userId))
            {
                return NotFound("User not found");
            }
            else
            {
                var product = await _unitOfWork.ProductTemplates.Get(p => p.Id == productId, include: p => p.Include(t => t.Participants).Include(t => t.InsuranceObject)
                                                                                                            .ThenInclude(t => t.InsuranceObjects).ThenInclude(t => t.PropertyDetails)
                                                                                                            .ThenInclude(t => t.Details));

                product.Risk = await _unitOfWork.Risks.Get(p => p.Id == product.RisksSectionId, include: p => p.Include(t => t.RiskGroups).ThenInclude(t => t.Risks).ThenInclude(t => t.Exclusions));
                product.Limit = await _unitOfWork.Limits.Get(p => p.Id == product.LimitSectionId, include: p => p.Include(t => t.Limits).ThenInclude(t => t.SubLimits));
                product.Rate = await _unitOfWork.Rates.Get(p => p.Id == product.RatesSectionId, include: p => p.Include(t => t.Rates).ThenInclude(t => t.RateOptions).ThenInclude(t => t.Conditions).ThenInclude(t => t.ConditionValues));
                product.Planning = await _unitOfWork.Plannings.Get(p => p.Id == product.PlanningSectionId, include: p => p.Include(t => t.PlanningTerms));
                var stages = await _unitOfWork.ProductStages.GetAll(p => p.ProductTemplateId == product.Id, include: p => p.Include(p => p.Templates).ThenInclude(p => p.Files).Include(p => p.Attachments));
                product.ProductStages = await stages.ToListAsync();

                if (product == null)
                    return NotFound("Product is not found");                    

                return Ok(product);
            }
        }

        [HttpGet("userapplications")]
        public async Task<IActionResult> GetUserApplications([FromQuery] RequestParams requestParams)
        {
            var userId = _httpContext?.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (string.IsNullOrWhiteSpace(userId))
            {
                return NotFound("User not found");
            }
            else
            {
                var user = _dbContext.Users.FirstOrDefault(u => u.Id.ToString() == userId);

                if (user == null)
                {
                    return NotFound("User not found");
                }

                var seller = await _dbContext.Sellers.Where(s => s.Id == user.SellerId).Include(s => s.Employees).FirstOrDefaultAsync();

                if (seller == null)
                {
                    return NotFound("Seller not found");
                }

                var sellerEmployeeIds = new List<string>();


                foreach (var sEmployee in seller.Employees)
                {
                    if (sEmployee != null)
                    {
                        if (sEmployee.Id != null && !sEmployee.IsDeleted)
                        {
                            sellerEmployeeIds.Add(sEmployee.Id.ToString());
                        }
                    }
                }

                var applications = await _unitOfWork.Applications.GetPagedList(requestParams, p => !p.IsDeleted && sellerEmployeeIds.Contains(p.CreatedBy),
                orderBy: p => p.OrderByDescending(p => p.Id), include: p => p.Include(t => t.Participants));

                var respose = _mapper.Map<ResponseDTO>(applications);
                respose.Data = _mapper.Map<IEnumerable<UserApplication>>(applications);

                foreach (var application in (IEnumerable<UserApplication>)respose.Data)
                {
                    application.Product = (await _unitOfWork.ProductTemplates.Get(p => p.Id == application.ProductTemplateId))?.Title;
                    //application.InsuranceSum = (await _unitOfWork.ProductTemplates.Get(p => p.Id == application.ProductTemplateId))?.TotalInsuranceSum ?? Decimal.Zero;
                    //application.InsurancePremium = (await _unitOfWork.ProductTemplates.Get(p => p.Id == application.ProductTemplateId))?.TotalInsurancePremium ?? Decimal.Zero;
                    application.Client = (await CounteragentService.GetCounteragentByIdAsync(_httpClient, application.CounteragentId, _appConfig))?.Title;

                }

                return Ok(respose);
            }
        }
    }
}
