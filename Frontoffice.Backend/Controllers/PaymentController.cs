﻿using CRM.Common;
using CRM.Data.Enums;
using Frontoffice.Backend.Services;
using Frontoffice.Repository.DTO.Billing;
using Microsoft.AspNetCore.Mvc;

namespace Frontoffice.Backend.Controllers
{
    
    
    [Produces(ApiResponseType.JsonResponse)]
    public class PaymentController : BaseApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly HttpClient _httpClient;
        private readonly PaymentService _paymentService;

        public PaymentController (IMapper mapper, IUnitOfWork unitOfWork, PaymentService paymentService)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _paymentService = paymentService ?? throw new ArgumentNullException(nameof(paymentService));
        }

        [HttpPost("/payme/payment/pay")]
        public async Task<IActionResult> PayByBill(PayBillDTO payData)
        {
            return Ok(await _paymentService.Pay(payData.BillId, payData.ChargedSum, payData.PhoneNumber, payData.Url));
        }
    
        //[HttpPost("payment/save")]
        //public async Task<ActionResult> SavePayment(long billId)
        //{
        //    var response = await _paymentService.CreateReceipt(billId); 
        //    await _paymentService.SendReceipt(response.receiptId, response.phoneNumber);
        //    return Ok(response.receiptId);
        //}
    
        [HttpPost("/payme/payment/createform")]
        public async Task<IActionResult> CreatePayment(long billId)
        {
            return Ok(await _paymentService.CreatePayment(billId));
        }

        [HttpPost("/payme/transaction/create")]
        public async Task<IActionResult> CreateTransaction(long chequeId, string receiptId, long time)
        {
            return Ok(await _paymentService.CreateTransaction(chequeId, receiptId, time));
        }

        [HttpPost("/payme/transaction/perform")]
        public async Task<IActionResult> PerformTransaction([FromBody] string receiptId)
        {
            return Ok(await _paymentService.PerformTransaction(receiptId));
        }

        [HttpPost("/payme/transaction/check")]
        public async Task<IActionResult> CheckTransaction([FromBody] string receiptId)
        {
            return Ok(await _paymentService.GetBill(receiptId));
        }

        [HttpPost("/payme/transaction/cancel")]
        public async Task<IActionResult> CancelTransaction(string transactionId, TransactionResult reason)
        {
            return Ok(await _paymentService.CancelTransaction(transactionId, reason));
        }
    }
}
