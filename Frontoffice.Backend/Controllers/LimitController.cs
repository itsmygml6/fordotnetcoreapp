﻿using Constructor.Backend.Services;
using Microsoft.AspNetCore.Mvc;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LimitController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public LimitController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        [HttpPost("validate")]
        public async Task<bool> ValidateLimit(LimitValidationDTO dto)
        {
            return await LimitService.CalculateLimits(dto, _unitOfWork);
        }
    }
}
