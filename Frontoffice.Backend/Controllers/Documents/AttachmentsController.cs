﻿using System.Net.Http.Headers;
using AutoMapper;
using CRM.Common.Rest;
using CRM.Common.Extensions;
using CRM.Constructor.DTO;
using CRM.Data.Models;
using CRM.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;
using System.Text.RegularExpressions;
using Minio;
using Microsoft.Extensions.FileProviders;
using Frontoffice.Backend.Services;
using Frontoffice.Repository.DTO;
using Frontoffice.Repository.UnitOfWork;
using Minio.DataModel;
using CRM.Common;

namespace Frontoffice.Backend.API
{
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    public class AttachmentsController : ControllerBase
    {
        private static readonly string MinioFolderName = "documents";
        
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly MinioService _minio;
        private readonly AppConfig _config;


        public AttachmentsController(IMapper mapper, IUnitOfWork unitOfWork, MinioService minio, AppConfig config)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _minio = minio ?? throw new ArgumentNullException(nameof(minio));
            _config = config ?? throw new ArgumentNullException(nameof(config));
        }

        [HttpGet("{id}")]
        public async Task<FileStreamResult> Download(long id)
        {
            var attachment = await _unitOfWork.Attachments.Get(p => p.Id == id);
            if (attachment == null)
            {
                return null;
            }

            Stream fileStream = await _minio.Download(_config.MinIoBucket, MinioFolderName, attachment.Name);
            fileStream.Position = 0;

            return File(fileStream, "application/" + attachment.Name.Substring(attachment.Name.LastIndexOf('.')).Trim('.'), attachment.Name);
        }
        
        [HttpGet("guid/{guid}")]
        public async Task<FileStreamResult> DownloadByGuid(string guid) 
        {
            Stream fileStream = await _minio.Download(_config.MinIoBucket, MinioFolderName, guid);
            fileStream.Position = 0;
            
            var stat = await _minio.GetStats(_config.MinIoBucket, MinioFolderName, guid);

            return File(fileStream, stat.ContentType, guid);
        }
        
        [HttpGet("stat/{id}")]
        public async Task<ObjectStat> GetStat(long id)
        {
            var attachment = await _unitOfWork.Attachments.Get(p => p.Id == id);
            if (attachment == null)
            {
                return null;
            }

            return await _minio.GetStats(_config.MinIoBucket, MinioFolderName, attachment.Name);
        }

        [HttpPost("save")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> UploadWithSave(IFormFile file)
        {
            Stream stream = file.OpenReadStream();
            if (stream.Length > _config.MaxFileSize)
            {
                return BadRequest("Invalid file size.");
            }
            string guid = await _minio.Upload(_config.MinIoBucket, MinioFolderName, file);
            string fileName = file.FileName;

            string fileExtension = fileName.Substring(fileName.LastIndexOf('.'));
            
            var attachment = new Attachment()
            {
                Id = 0,
                Name = guid + fileExtension,
                Title = fileName,
                Extension = fileExtension,
                Size = file.Length
            };
            await _unitOfWork.Attachments.Insert(attachment);
            await _unitOfWork.Save();

            return Ok(attachment);
        }

        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            Stream stream = file.OpenReadStream();
            if (stream.Length > _config.MaxFileSize)
            {
                return BadRequest("Invalid file size.");
            }
            string guid = await _minio.Upload(_config.MinIoBucket, MinioFolderName, file);
            
            return Ok(guid);
        }

        [HttpPost("saveuploaded")]
        [DisableRequestSizeLimit]
        public async Task<ActionResult> SaveFile(IEnumerable<AttachmentUploadedCreationDTO> attachments)
        {
            if (attachments is null)
            {
                return BadRequest("No attachments.");
            }

            foreach (var i in attachments)
            {
                var tempAttachment = _mapper.Map<Attachment>(i);
                var meta = await _minio.GetStats(_config.MinIoBucket, MinioFolderName, i.Name);
                tempAttachment.Size = meta.Size;
                tempAttachment.Extension = "pdf";
                try
                {
                    await _unitOfWork.Attachments.Insert(tempAttachment);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var attachment = await _unitOfWork.Attachments.Get(p => p.Id == id);
            if (attachment == null)
            {
                return NotFound("Document file is not found");
            }

            await _minio.Delete(_config.MinIoBucket, MinioFolderName, attachment.Name);
            
            attachment.IsDeleted = true;
            _unitOfWork.Attachments.Update(attachment);
            await _unitOfWork.Save();
            return NoContent();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(ICollection<string> guids)
        {
            if (guids is null)
            {
                return NotFound("Document file is not found");
            }

            foreach (var guid in guids)
            {
                try
                {
                    await _minio.Delete(_config.MinIoBucket, MinioFolderName, guid);
                }
                catch
                {
                    continue;
                }
            }
            
            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(long id, IFormFile file)
        {
            var attachment = await _unitOfWork.Attachments.Get(p => p.Id == id);
            if (attachment == null)
            {
                return NotFound("Document file is not found");
            }
            
            await _minio.Delete(_config.MinIoBucket, MinioFolderName, attachment.Name);
            var guid = await _minio.Upload(_config.MinIoBucket, MinioFolderName, file);
            
            attachment.Title = file.FileName;
            attachment.Name = guid + file.FileName.Substring(file.FileName.LastIndexOf('.'));
            _unitOfWork.Attachments.Update(attachment);
            await _unitOfWork.Save();
            return Ok(attachment);
        }
    }
}
