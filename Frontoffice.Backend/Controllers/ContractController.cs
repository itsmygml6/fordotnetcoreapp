﻿using CRM.Common;
using CRM.Constructor.DTO;
using CRM.Data;
using CRM.Data.Enums;
using Frontoffice.Backend.Extensions;
using Frontoffice.Backend.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    public class ContractController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly HttpClient _httpClient;
        private readonly AppConfig _configuration;
        private readonly IHttpContextAccessor _httpContext;
        private readonly AppDbContext _context;

        public ContractController(IMapper mapper, IUnitOfWork unitOfWork, HttpClient httpClient, AppConfig configuration, IHttpContextAccessor httpContext, AppDbContext context)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] RequestParams requstParams)
        {
            var userId = _httpContext?.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (string.IsNullOrWhiteSpace(userId))
            {
                return NotFound("User not found");
            }
            try
            {
                var users = await FunctionalExtensions.GetSellerUsersByUserIdAsync(_context, userId);
                var contracts = await _unitOfWork.Contracts.GetPagedList(requstParams, c => !c.IsDeleted && users.Contains(c.CreatedBy));
                var response = _mapper.Map<ResponseDTO>(contracts);
                response.Data = _mapper.Map<IEnumerable<ContractTableDTO>>(contracts);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(long Id)
        {
            var contract = await _unitOfWork.Contracts.Get(c => c.Id == Id, include: p => p
                .Include(t => t.Attachments.Where(d => !d.IsDeleted)));
            if(contract == null)
                return NotFound("Contract is not found");

            var response = _mapper.Map<ContractDTO>(contract);
            response.Counteragent = (await CounteragentService.GetCounteragentByIdAsync(_httpClient, response?.CounteragentId, _configuration))?.Title;
            var user = await IdentityService.GetUserInfoAsync(_httpClient, response?.SignerOwnId, _configuration);
            response.SignerOwn = user?.FirstName + " " + user?.LastName;
            response.DealNumber = (await _unitOfWork.Deals.Get(d => d.ContractId == contract.Id))?.Number;

            return Ok(response);
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> Update(long Id,ContractDTO contractDTO)
        {
            var contract = await _unitOfWork.Contracts.Get(c => c.Id == Id);
            if (contract == null)
                return NotFound("contact is not found");

            _mapper.Map(contractDTO, contract);
            contract.Id = Id;
            _unitOfWork.Contracts.Update(contract);
            await _unitOfWork.Save();
            return Ok(contract);
        }

        [HttpGet("Cancel/{Id}")]
        public async Task<IActionResult> Cancel(long Id)
        {
            var contract = await _unitOfWork.Contracts.Get(c => c.Id == Id);
            if (contract == null)
                return NotFound("contact is not found");

            contract.State = ContractState.Canceled;
            _unitOfWork.Contracts.Update(contract);
            await _unitOfWork.Save();
            return Ok(contract);
        }

        [HttpPost]
        public async Task<IActionResult> Create(ContractDTO contractDTO)
        { 
            var contract = _mapper.Map<Contract>(contractDTO);

            await _unitOfWork.Contracts.Insert(contract);
            await _unitOfWork.Save();
            
            var deal = await _unitOfWork.Deals.Get(d => d.Number == contractDTO.DealNumber);
            if (deal != null)
            {
                deal.ContractId = contract.Id;
                _unitOfWork.Deals.Update(deal);
            }


            await _unitOfWork.Save();
            return Ok(contract);
        }
    }
}
