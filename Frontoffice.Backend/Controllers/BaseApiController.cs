﻿global using CRM.Common.Rest;
global using CRM.Data.Models;
global using Frontoffice.Repository.UnitOfWork;
global using AutoMapper;
global using Frontoffice.Repository.DTO;
using Microsoft.AspNetCore.Mvc;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    public abstract class BaseApiController : ControllerBase
    {
        protected new IActionResult Ok(object value = null!)
        {
            var response = new ApiResponse
            {
                Data = value,
                Success = true
            };

            return base.Ok(response);
        }
    }
}
