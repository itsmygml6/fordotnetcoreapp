﻿using System.Collections;
using System.Security.Claims;
using CRM.Common;
using CRM.Constructor.DTO;
using CRM.Data;
using Frontoffice.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    public class ProductInformationController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly HttpClient _httpClient;
        private readonly AppConfig _configuration;
        private readonly IHttpContextAccessor _httpContext;
        private readonly AppDbContext _context;

        public ProductInformationController(IMapper mapper, IUnitOfWork unitOfWork, HttpClient httpClient, AppConfig configuration, IHttpContextAccessor httpContext, AppDbContext context)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        [HttpGet]
        public async Task<ActionResult> Get([FromQuery] RequestParams request)
        {
            var userId = _httpContext?.HttpContext?.User?.FindFirst("preferred_username")?.Value;
            if (string.IsNullOrWhiteSpace(userId))
            {
                return NotFound("User not found.");
            }

            var products = await _unitOfWork.ProductTemplates.GetPagedList(request, p => p
                .UserLogin == userId && !p.IsDeleted);
            if (products is null)
            {
                return NotFound("Product not found.");
            }

            var productsTable = _mapper.Map<ResponseDTO>(products);
            productsTable.Data = _mapper.Map<IEnumerable<ProductInformationDTO>>(products);

            return Ok(productsTable);
        }
    }
}
