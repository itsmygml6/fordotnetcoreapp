﻿using CRM.Common;
using CRM.Common.Entities;
using Frontoffice.Backend.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Frontoffice.Backend.Controllers
{
    public class AuthController : ControllerBase
    {
        private readonly AppConfig _config;
        private readonly HttpClient _httpClient;
        private readonly UserService userService;

        public AuthController(AppConfig config, HttpClient httpClient, UserService userService)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            this.userService = userService;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Auth([FromBody] Login login)
        {
            if (string.IsNullOrWhiteSpace(login.Username))
            {

                if (string.IsNullOrWhiteSpace(login.Username) || string.IsNullOrWhiteSpace(login.Password))
                    throw new Exception($"Login or password is null");
            }

            var content = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8,
                ApiResponseType.JsonResponse);

            var response = await _httpClient.PostAsync($"{_config.TokenUrl}/Account/Login?client={_config.ClientId}", content);



            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(response.ReasonPhrase);
            }
            var json = await response.Content.ReadAsStringAsync();
            var tokenData = JsonConvert.DeserializeObject<ApiResponse<AuthInfo>>(json);


            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(tokenData.Data.AccessToken);

            var userId = jwtSecurityToken.Claims.First(i => i.Type == "sub").Value;

            var user = await userService.GetUser(userId);
            if (user == null)
            {
                var userInfo = await IdentityService.GetUserInfoAsync(_httpClient, userId, _config);
                if (userInfo?.Role == null)
                    throw new Exception("User cannot access on system");
            }

            if (!user.IsActive || user.SellerId == null)
            {
                throw new Exception("User cannot access on system");
            }

            return Ok(tokenData);
        }

        [HttpPost]
        [Route("token")]
        public async Task<IActionResult> RefreshToken([FromBody] Token token)
        {
            var content = new StringContent(JsonConvert.SerializeObject(token), Encoding.UTF8,
                ApiResponseType.JsonResponse);

            var response = await _httpClient.PostAsync($"{_config.TokenUrl}/Account/Token?client={_config.ClientId}", content);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(response.ReasonPhrase);
            }

            var json = await response.Content.ReadAsStringAsync();

            return Ok(JsonConvert.DeserializeObject(json));
        }

        [HttpGet("userdata/{login}")]
        public async Task<ActionResult> GetUserData(string login)
        {
            var response = await _httpClient.GetAsync(_config.TokenUrl + "/User/GetByUsername/" + login);
            if (!response.IsSuccessStatusCode)
            {
                return NotFound("User not found.");
            }

            var data = JsonConvert.DeserializeObject<ApiResponse<UserInfo>>(await response.Content.ReadAsStringAsync());
            return Ok(data.Data);
        }

        public class Token
        {
            public string? RefreshToken { get; set; }
        }
        public class Login
        {
            public string? Username { get; set; }
            public string? Password { get; set; }
        }

        public class UserInfo
        {
            public string Id { get; set; } = String.Empty;
            public string Username { get; set; } = String.Empty;
            public string Email { get; set; } = String.Empty;
            public string FirstName { get; set; } = String.Empty;
            public string LastName { get; set; } = String.Empty;
            public object FathersName { get; set; } = String.Empty;
            public object Password { get; set; } = String.Empty;
            public bool TempPassword { get; set; }
            public string WorkPhone { get; set; } = String.Empty;
            public string CellPhone { get; set; } = String.Empty;
            public string Role { get; set; } = String.Empty;
            public bool IsBlocked { get; set; }
        }
    }
}
