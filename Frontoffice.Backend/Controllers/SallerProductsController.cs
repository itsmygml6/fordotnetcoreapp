﻿using Microsoft.EntityFrameworkCore;
using CRM.Constructor.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using CRM.Data;
using Microsoft.AspNetCore.Authorization;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    [Authorize]
    public class SallerProductsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContext;
        private readonly AppDbContext _context;

        public SallerProductsController(IUnitOfWork unitOfWork, IMapper mapper, IHttpContextAccessor httpContext, AppDbContext context)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        [HttpGet]
        public async Task<IActionResult> GetProductsBySellerId([FromQuery] RequestParams request)
        {
            var userId = _httpContext?.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (string.IsNullOrWhiteSpace(userId))
            {
                return NotFound("User not found");
            }

            var user = await _context.Users.FirstOrDefaultAsync(p => p.Id.ToString() == userId);

            if (user == null)
            {
                return NotFound("User not found");
            }            
            
            var products = await _unitOfWork.SellerProducts.GetPagedList(request, p => !p.IsDeleted && p.SellerId == user.SellerId, 
                include: p => p.Include(p => p.Product));

            var response = _mapper.Map<ResponseDTO>(products);
            response.Data = products.Select(p => new
                {Title = p.Product?.Title, Description = p.Product?.Description, CreatedTime = p.CreatedDate});

            return Ok(response);
        }
    }
}
