﻿using Frontoffice.Backend.Services;
using Microsoft.AspNetCore.Mvc;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    public class VehicleController : ControllerBase
    {
        private readonly OsagoService _osagoService;
        public VehicleController(OsagoService osagoService)
        {
            _osagoService = osagoService;
        }

        [HttpPost("get")]
        public async Task<IActionResult> Get(VehicleDTO dto)
        {
            try
            {
                var data = await _osagoService.GetVehicleAsync(dto);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
