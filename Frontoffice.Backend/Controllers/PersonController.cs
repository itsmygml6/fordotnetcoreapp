﻿using Frontoffice.Backend.Services;
using Microsoft.AspNetCore.Mvc;

namespace Frontoffice.Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    public class PersonController : ControllerBase
    {
        private readonly OsagoService _osagoService;
        public PersonController(OsagoService osagoService)
        {
            _osagoService = osagoService;
        }

        [HttpPost("get")]
        public async Task<IActionResult> Get(PersonDTO dto)
        {
            try
            {
                var data = await _osagoService.GetPersonAsync(dto);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("getCompany")]
        public async Task<IActionResult> GetCompany(CompanyDTO dto)
        {
            try
            {
                var data = await _osagoService.GetCompanyAsync(dto);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
