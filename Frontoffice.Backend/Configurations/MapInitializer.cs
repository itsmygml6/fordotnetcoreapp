using AutoMapper;
using CRM.Data.Models;
using Frontoffice.Backend.Services;
using Frontoffice.Repository;
using Frontoffice.Repository.DTO;
using X.PagedList;
using Frontoffice.Repository.DTO.Billing;

namespace Frontoffice.Backend.Configurations
{
    public class MapInitializer : Profile
    {
        public MapInitializer()
        {
            #region DTO
            CreateMap<IPagedList, ResponseDTO>().ForMember(p => p.Total, o => o.MapFrom(p => p.TotalItemCount)).ReverseMap();
            #region Information
            CreateMap<ProductTemplate, ProductResponse>()
                .ForMember(p => p.Classes, p => p.MapFrom(t => ProductInformation.GetInsuranceClasses(t.Classes)))
                .ForMember(p => p.Stages, p => p.MapFrom(t => ProductInformation.GetStages(t.ProductStages)))
                .ForMember(p => p.Type, p => p.MapFrom(t => t.Type.Title)).ReverseMap();
            CreateMap<Application, UserApplication>()
                .ForMember(p => p.CounteragentId, p => p.MapFrom(t => t.Participants.FirstOrDefault(k => k.Role == CRM.Data.Enums.RoleType.Insurant).CounteragentId))
                .ReverseMap();
            CreateMap<ProductTemplate, ProductInformationDTO>();
            #endregion
            
            #region Incoming

            CreateMap<ApplicationIncomingDTO, Application>()
                .ForMember(p => p.Rates, d => d.MapFrom(q => q.RatesSection.Rates))
                .ForMember(p => p.Limits, d => d.MapFrom(q => q.LimitSection.Limits))
                .ForMember(p => p.Risks, d => d.MapFrom(q => q.RisksSection.Risks))
                .ForMember(p => p.Objects, d => d.MapFrom(q => q.InsuranceObjectSection.Objects));
            CreateMap<ApplicationParticipantDTO, ApplicationParticipant>();
            CreateMap<PaymentSourceTypeDTO, PaymentSourceType>();
            CreateMap<PaymentEventDTO, PaymentEvent>();
            CreateMap<PaymentPeriodDTO, PaymentPeriod>();
            CreateMap<PaymentTermDTO, PaymentTerm>();
            CreateMap<InsuranceScheduleDTO, InsuranceSchedule>();
            CreateMap<ApplicationRateDTO, ApplicationRate>();
            CreateMap<ApplicationLimitDTO, ApplicationLimit>();
            CreateMap<ApplicationRiskDTO, ApplicationRisk>();
            CreateMap<ApplicationRiskExclusionsDTO, ApplicationRiskExclusion>();
            CreateMap<ApplicationInsuranceObjectDTO, ApplicationObject>();
            CreateMap<ObjectDetailsTemplateDTO, ObjectDetailsList>();
            CreateMap<Bill, BillDTO>().ReverseMap();
            CreateMap<BillCreation, Bill>();
            CreateMap<Bill, BillCreation>();
            CreateMap<AttachmentDTO, AttachmentType>();
            CreateMap<ContractDTO, Contract>();
            CreateMap<ContractTableDTO, Contract>();
            CreateMap<Contract, ContractTableDTO>();
            CreateMap<Contract, ContractDTO>();
            CreateMap<Attachment, ContractAttachmentDTO>();
            CreateMap<ContractAttachmentDTO, Attachment>();
            CreateMap<Attachment, AttachmentUploadedCreationDTO>();
            CreateMap<AttachmentUploadedCreationDTO, Attachment>();

            #endregion

            #endregion

            #region Select

            CreateMap<InsuranceClass, Select>().ReverseMap();
            CreateMap<ProductTemplate, Select>().ReverseMap();
            CreateMap<ProductType, Select>().ReverseMap();

            #endregion
        }
    }
}
