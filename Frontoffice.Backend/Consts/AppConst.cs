﻿namespace Frontoffice.Backend.Consts
{
    public struct AppConst
    {
        public const string DefaultCulture = "uz";

        public const int LocaleCountry = 1;

        public const string BaseUrlCounterAgent = "http://172.28.210.166:15021/rest/";
    }
}
