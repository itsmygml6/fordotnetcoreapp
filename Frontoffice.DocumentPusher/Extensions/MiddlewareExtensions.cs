
using Frontoffice.DocumentPusher.Handlers;

namespace Frontoffice.DocumentPusher.Extensions
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseAppException(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
