﻿using CRM.Common;
using CRM.Data.Core;
using Microsoft.EntityFrameworkCore;

namespace Frontoffice.DocumentPusher.Services
{
    public class PolicyRegistratorService
    {
        private readonly IAppDbContext _dbContext;
        private readonly AppConfig _appConfig;
        public PolicyRegistratorService(IAppDbContext dbContext, AppConfig appConfig)
        {
            _dbContext = dbContext;
            _appConfig = appConfig;
        }

        public async Task StartPolicyRegistrationProccess()
        {
            if(_appConfig.BaseDocumentRegistrationUrl == null)
            {
                throw new Exception("Config file value for BaseDocumentRegistrationUrl is absent");
            }
            PolicyRequestService requestService = new(_appConfig.BaseDocumentRegistrationUrl);

            var policyCollection = await _dbContext.Policies.
                Include(d => d.Deal).
                Where(p => (p.State == CRM.Data.Enums.SalesPolicyState.Created)
                && (p.Deal.Stages == CRM.Data.Enums.SalesDealStages.Policy)
                && p.CreateDate < DateTime.Now.AddMinutes(-15)).ToListAsync();

            foreach (var policy in policyCollection)
            {
                await requestService.SendPolicyid(policy.Id);
            }
        }
    }
}
