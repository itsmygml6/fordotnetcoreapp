﻿using CRM.Common.Actions;
using CRM.Common.Rest;

namespace Frontoffice.DocumentPusher.Services
{
    public class PolicyRequestService : ApiClient
    {
        public PolicyRequestService(string baseUrl) : base(baseUrl)
        {
        }
        public async Task<ApiResponse> SendPolicyid(long id)
        {
            
            return await PostNoResult($"/registration/policy?policyId={id}", new { });
        }
    }
}
