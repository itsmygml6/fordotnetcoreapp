﻿using CRM.Common;
using CRM.Data.Core;
using CRM.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Frontoffice.DocumentPusher.Services
{
    public class DocumentCloserService
    {
        private readonly IAppDbContext _dbContext;
        public DocumentCloserService(IAppDbContext dbContext, AppConfig appConfig)
        {
            _dbContext = dbContext;
        }
        public async Task StartClosingProccess()
        {
            await StartPolicyCloser();
            await StartContractCloser();
            await StartApplicationCloser();
        }

        public async Task StartPolicyCloser()
        {
            var policyCollection = await _dbContext.Policies.
                Include(d=>d.Deal).
                Where(p => (p.State == CRM.Data.Enums.SalesPolicyState.Issued) 
                && (p.Deal.Stages == CRM.Data.Enums.SalesDealStages.Monitoring)
                && p.EndDate.Date < DateTime.Now.Date).ToListAsync();

            foreach (var policy in policyCollection)
            {
                policy.State = CRM.Data.Enums.SalesPolicyState.Finished;
            }
            await _dbContext.SaveChangesAsync();
        }

        public async Task StartContractCloser()
        {
            var contractCollection = await _dbContext.Contracts.
                Include(d => d.Deal).
                Where(p => (p.State == CRM.Data.Enums.ContractState.Issued)
                && (p.Deal.Stages == CRM.Data.Enums.SalesDealStages.Monitoring)
                && p.DueDate.Date < DateTime.Now.Date).ToListAsync();

            foreach (var policy in contractCollection)
            {
                policy.State = CRM.Data.Enums.ContractState.Completed;
            }
            await _dbContext.SaveChangesAsync();
        }

        public async Task StartApplicationCloser()
        {
            var applicationCollection = await _dbContext.Applications.
                Include(d => d.Deal).
                Where(p => (p.State == CRM.Data.Enums.State.Active)
                && (p.Deal.Stages == CRM.Data.Enums.SalesDealStages.Monitoring)
                && p.InsuranceEnd.Date < DateTime.Now.Date).ToListAsync();

            foreach (var policy in applicationCollection)
            {
                policy.State = CRM.Data.Enums.State.Disabled;
            }
            await _dbContext.SaveChangesAsync();
        }
    }
}
