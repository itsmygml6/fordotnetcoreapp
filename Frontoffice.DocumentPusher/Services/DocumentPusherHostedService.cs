﻿using CRM.Common;
using CRM.Data.Core;
using Frontoffice.Repository.UnitOfWork;

namespace Frontoffice.DocumentPusher.Services
{
    public class DocumentPusherHostedService : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly IServiceProvider _services;
        private DateTime _lastUpdate;
        private DateTime _lastUpdateDay;
        private DocumentCloserService _documentCloserService = null;
        private PolicyRegistratorService _policyRegistratorService = null;
        public DocumentPusherHostedService(ILogger<DocumentPusherHostedService> logger, IServiceProvider services)
        {
            _services = services;
            _logger = logger;
            _lastUpdate = DateTime.Now;
            _lastUpdateDay = DateTime.Now.Date;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            var scope = _services.CreateScope();
            _documentCloserService = scope.ServiceProvider.GetRequiredService<DocumentCloserService>();
            _policyRegistratorService = scope.ServiceProvider.GetRequiredService<PolicyRegistratorService>();

            _logger.LogInformation($"Document pusher is start.");
            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    if (_lastUpdate < DateTime.Now.AddMinutes(-15))
                    {
                        await _policyRegistratorService.StartPolicyRegistrationProccess();
                        _lastUpdate = DateTime.Now;
                    }

                    if (_lastUpdateDay.Date < DateTime.Now.Date )
                    {
                        await _documentCloserService.StartClosingProccess();
                        _lastUpdateDay = DateTime.Now.Date;
                    }

                    await Task.Delay(1000, stoppingToken);
                }
                catch (OperationCanceledException)
                {
                    _logger.LogInformation($"");
                }
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
