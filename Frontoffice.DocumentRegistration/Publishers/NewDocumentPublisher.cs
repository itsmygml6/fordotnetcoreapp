﻿using Frontoffice.RabbitMQ.Core;
using Newtonsoft.Json;
using RabbitMQ.Client;  
using System.Text;
using CRM.Common;

namespace Trading.Receiver.API.Publishers
{
    public class NewDocumentPublisher
    {
        private readonly IModel _channel;
        private readonly ILogger<NewDocumentPublisher> _logger;   

        public NewDocumentPublisher(ILogger<NewDocumentPublisher> logger, AppConfig config, RabbitClient client)
        {
            _logger = logger; 
            _channel = client.CreateChannel(config.RabbitMQConnection, config.RabbitMQUser, config.RabbitMQPassword);
        }

        public void Publish(string bid)
        {
            _channel.ExchangeDeclare(RabbitKeys.NEW_DOCUMENT_QUEUE, ExchangeType.Direct);

            var body = Encoding.UTF8.GetBytes(bid);
            _channel.BasicPublish(RabbitKeys.NEW_DOCUMENT_QUEUE, "", null, body);
        }
    }
}
