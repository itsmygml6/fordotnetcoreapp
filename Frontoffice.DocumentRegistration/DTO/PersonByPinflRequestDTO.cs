﻿namespace Frontoffice.DocumentRegistration.DTO
{
    public class PersonByPinflRequestDTO
    {
        public string pinfl { get; set; }

        public string passportSeries { get; set; }

        public string passportNumber { get; set; }
    }
}
