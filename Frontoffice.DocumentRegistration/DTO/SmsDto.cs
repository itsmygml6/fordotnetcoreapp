﻿using Newtonsoft.Json;

namespace Frontoffice.DocumentRegistration.DTO
{
    public class SmsMessages
    {
        [JsonProperty("messages")]
        public List<SmsMessage> Messages { get; set; }
    }
    public class SmsMessage 
    {
        [JsonProperty("recipient")]
        public string Recipient { get; set; }
        [JsonProperty("message-id")]
        public string MessageId { get; set; }
        [JsonProperty("sms")]
        public SmsDto Sms { get; set; }


    }
    public class SmsDto
    {
        [JsonProperty("originator")]
        public string Originator { get; set; }
        [JsonProperty("content")]
        public ContentSms Content { get; set; }
    }

    public class ContentSms
    {
        [JsonProperty("text")]
        public string Text { get; set; }
    }


}
