﻿using Newtonsoft.Json;

namespace Frontoffice.DocumentRegistration.DTO
{
    public class DriverLicenseDto
    {
        [JsonProperty("licenseNumber")]
        public string? LicenseNumber { get; set; }

        [JsonProperty("licenseSeria")]
        public string? LicenseSeria { get; set; }

        [JsonProperty("issueDate")]
        public DateTime IssueDate { get; set; }

        [JsonProperty("pOwner")]
        public string? POwner { get; set; }

        [JsonProperty("pOwnerDate")]
        public string? POwnerDate { get; set; }

        [JsonProperty("modelDL")]
        public List<string>? ModelDL { get; set; }
    }
}
