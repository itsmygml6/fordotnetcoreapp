﻿using CRM.Common.Rest;
using CRM.Data.Core;
using CRM.Data.Models;
using Frontoffice.DocumentRegistration.Extensions;
using Frontoffice.DocumentRegistration.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Frontoffice.DocumentRegistration.Controllers
{    
    
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    //[Authorize]
    public class SmsController : BaseApiController
    {
        
        public SmsController()
        {
            
        }

        [HttpPost]
        [Route("send")]
        public async Task<IActionResult> SmsSend(string recipient, string message)
        {
            SmsService smsService = new SmsService();
            var result  = await smsService.SendSmsAsync(recipient, message);
            return Ok(result);
        }
    }
}
