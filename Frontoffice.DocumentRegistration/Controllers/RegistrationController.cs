﻿using CRM.Common.Rest;
using CRM.Data.Core;
using CRM.Data.Models;
using Frontoffice.DocumentRegistration.DTO;
using Frontoffice.DocumentRegistration.Extensions;
using Frontoffice.DocumentRegistration.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Frontoffice.DocumentRegistration.Controllers
{    
    
    [Route("[controller]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    //[Authorize]
    public class RegistrationController : BaseApiController
    {
        private readonly PolicyRegistrationService _policyRegistartionService;
        private readonly IdentityService identityService;
        private readonly FoundService _foundService;

        public RegistrationController(PolicyRegistrationService policyRegistrationService, IdentityService identityService, FoundService foundService)
        {
            _policyRegistartionService = policyRegistrationService;
            this.identityService = identityService;
            _foundService = foundService;
        }

        [HttpPost]
        [Route("policy")]
        public async Task<IActionResult> RegisterPolicy(long policyId)
        {
            var token = await identityService.GetAccessToken();

            await _policyRegistartionService.RegisterPolicy(policyId, token);
            return Ok();
        }

        [HttpPost]
        [Route("get-license")]
        public async Task<IActionResult> GetLicense(PersonByPinflRequestDTO driverParams)
        {
            var license = _foundService.GetDriverLicense(driverParams).Result;
                        
            return Ok(license);
        }
    }
}
