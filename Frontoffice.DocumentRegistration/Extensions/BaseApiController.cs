using System.Security.Claims;
using CRM.Common.Rest;
using Microsoft.AspNetCore.Mvc;
using CRM.Common.Rest;

namespace Frontoffice.DocumentRegistration.Extensions
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Produces(ApiResponseType.JsonResponse)]
    public abstract class BaseApiController : ControllerBase
    {
 
        [ProducesDefaultResponseType(typeof(ApiResponse))]
        protected new IActionResult Ok(object value = null)
        {
            var response = new ApiResponse
            {
                Data = value,
                Success = true
            };

            return base.Ok(response);
        } 

        [ProducesDefaultResponseType(typeof(ApiResponse))]
        protected IActionResult BadRequest(string message ="")
        {
            var response = new ApiResponse
            { 
                Error = message 
            };

            return base.Ok(response);
        }

    }
}
