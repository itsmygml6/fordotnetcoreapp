
using Frontoffice.DocumentRegistration.Handlers;

namespace Frontoffice.DocumentRegistration.Extensions
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseAppException(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
