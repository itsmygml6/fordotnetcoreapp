﻿using CRM.Common;
using CRM.Common.Rest;
using Frontoffice.DocumentRegistration.DTO;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;

namespace Frontoffice.DocumentRegistration.Services
{
    public class FoundService 
    {
        private readonly AppConfig _appConfig;
        private readonly HttpClient _httpClient;
        public FoundService(AppConfig appConfig, HttpClient httpClient) 
        {
            _appConfig = appConfig;
            _httpClient = httpClient;
        }

        public async Task<DriverLicenseDto> GetDriverLicense(PersonByPinflRequestDTO driver)
        {
            var driverString = JsonConvert.SerializeObject(driver);

            var driverContent = new StringContent(driverString, Encoding.UTF8, "application/json"); //working code

            var response = await _httpClient.PostAsync($"{_appConfig.FoundIntegrationApi}/info/driverlicensebypinfl", driverContent);

            var responceContent = response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<ApiResponse>(await response.Content.ReadAsStringAsync()).Data : null;

            var resp = JsonConvert.DeserializeObject<DriverLicenseDto>(responceContent.ToString());

            return resp;
        }
    }
}
