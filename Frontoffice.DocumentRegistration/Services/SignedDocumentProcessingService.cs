﻿using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using Newtonsoft.Json;
using CRM.Common;
using Frontoffice.RabbitMQ.Core;
using Frontoffice.DocumentRegistration.Handlers;

namespace Frontoffice.DocumentRegistration.Services
{
    public class SignedDocumentProcessingService : IHostedService
    {
        private readonly IModel _channel;
        private readonly ILogger<SignedDocumentProcessingService> _logger;
        private PolicyRegistrationService _policyService;
        private readonly IServiceProvider _services;

        public SignedDocumentProcessingService(AppConfig config, ILogger<SignedDocumentProcessingService> logger, RabbitClient client, IServiceProvider services)
        {
            _logger = logger;
            _channel = client.CreateChannel(config.RabbitMQConnection, config.RabbitMQUser, config.RabbitMQPassword);
            _services = services;    
            
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            var scope = _services.CreateScope();
            _policyService = scope.ServiceProvider.GetRequiredService<PolicyRegistrationService>();

            //_channel.ExchangeDeclare(RabbitKeys.SIGNED_DOCUMENT_QUEUE, ExchangeType.Direct);

            //var queueName = _channel.QueueDeclare("yanarabbit");

            _channel.ExchangeDeclare(RabbitKeys.SIGNED_DOCUMENT_QUEUE, ExchangeType.Direct);
            _channel.QueueDeclare(RabbitKeys.SIGNED_DOCUMENT_QUEUE, true, false, false);
            


            _channel.QueueBind(RabbitKeys.SIGNED_DOCUMENT_QUEUE, RabbitKeys.SIGNED_DOCUMENT_QUEUE, "");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (sender, e) =>
            {
                await Task.Factory.StartNew(async (body) =>
                {
                    var message = Encoding.UTF8.GetString((byte[])body);
                    var commandRequest = JsonConvert.DeserializeObject<DocumentExchangeContainer<SignDocumentData>>(message);

                    var commandRequest2 = JsonConvert.DeserializeObject<SignDocumentData>(message);
                    
                    if (commandRequest.data != null)
                    {
                        await _policyService.UpdateDocumentState(commandRequest.data);
                    }
                    else
                    {
                        await _policyService.UpdateDocumentState(commandRequest2);
                    }


                }, e.Body.ToArray(), cancellationToken);
            };

            _channel.BasicConsume(RabbitKeys.SIGNED_DOCUMENT_QUEUE, true, consumer);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _channel.Close();

            return Task.CompletedTask;
        }
    }
}
