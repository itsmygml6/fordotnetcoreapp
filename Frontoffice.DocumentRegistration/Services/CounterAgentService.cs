﻿using APEX.Data.CounterAgent.Models;
using CRM.Common.Entities;
using CRM.Common.Rest;

namespace Frontoffice.DocumentRegistration.Services
{
    public class CounterAgentService:ApiClient
    {
        private readonly string _baseUrl;

        public CounterAgentService(string baseUrl) : base(baseUrl)
        {
            _baseUrl = baseUrl;
        }

        public async Task<Select?>? GetCounteragentByIdAsync(long counteragentId, string token = "")
        {
            var result = await GetSimple<Select>($"{_baseUrl}/rest/getcounteragent/{counteragentId}", token);

            return result;
        }

        public async Task<Person>? GetPersonByIdAsync(long counteragentId, string token = "")
        {
            var response = await GetSimple<Person>($"{_baseUrl}/person/{counteragentId}", token);
            
            return response;
        }

        public async Task<Organisation>? GetOrganisationByIdAsync(long counteragentId, string token = "")
        {
            var response = await GetSimple<Organisation>($"{_baseUrl}/organisation/get/{counteragentId}", token);

            return response;
        }
    }
}
