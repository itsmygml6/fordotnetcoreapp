﻿using CRM.Common;
using CRM.Common.Actions;
using CRM.Common.Rest;
using CRM.Data.Core;
using CRM.Data.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Text;
using RabbitMQ.Client;
using CRM.Data.Models.FrontOffice.Document.Entities.EPolis;
using CRM.Data.Models.FrontOffice.Document.Entities.HujjatUz;
using CRM.Data;
using CRM.Data.Models.FrontOffice.Document.Entities.EPolis.Dtos;
using Frontoffice.DocumentRegistration.Services.RabbitMQ.Producer;
using Trading.Receiver.API.Publishers;

namespace Frontoffice.DocumentRegistration.Services
{
    public class SendPolicyService
    {
        private readonly AppConfig _appConfig;
        private readonly HttpClient _httpClient;
        private readonly AppDbContext _dbContext;
        //private readonly NewDocumentPublisher _messagePublisher;
        private readonly IMessageProducer _messagePublisher;

        public SendPolicyService(AppConfig appConfig, HttpClient httpClient, AppDbContext appDbContext, IMessageProducer messageProducer)
        {
            _appConfig = appConfig;
            _httpClient = httpClient;
            _dbContext = appDbContext;
            _messagePublisher = messageProducer;
        }
                
        public async Task<ApiResponse?> SendToPolicyService(DocumentBody epolicy)
        {
            if (epolicy.Document.Type == CRM.Data.Enums.LegalizationType.EPolis)
            {
                /// testing start

                //StreamReader r = new StreamReader(@"c:\\policybody.json");
                //string jsonString = r.ReadToEnd();

                //var epolicyJson = JsonConvert.SerializeObject(jsonString);

                //var epolicyDataBody = new StringContent(jsonString, Encoding.UTF8, "application/json");

                /// testing end

                //var epolicyJson = JsonConvert.SerializeObject(epolicy.Body); //working code

                var epolicyDataBody = new StringContent(epolicy.Body, Encoding.UTF8, "application/json"); //working code

                var response = await _httpClient.PostAsync($"{_appConfig.FoundIntegrationApi}/epolicy/create", epolicyDataBody);
                                
                var responceContent = response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<ApiResponse>(await response.Content.ReadAsStringAsync()) : null;

                var payedResponseContent = new ApiResponse();

                if (responceContent != null)
                {
                    if (responceContent.Success)
                    {
                        var policy = await _dbContext.Policies.Where(p => p.Id == epolicy.PolicyId)
                            .Include(p => p.Deal)
                            .FirstOrDefaultAsync();

                        if (policy != null)
                        {
                            var policyUuid = JsonConvert.DeserializeObject<EPolicyResponce>(responceContent.Data.ToString());

                            var epolicyBody = JsonConvert.DeserializeObject<EPolisDocumentBody>(epolicy.Body);

                            var payedPolicy = new ConfirmPayedRequest
                            {
                                InsuranceFormUuid = policyUuid.uuid,
                                InsurancePremiumPaidToInsurer = epolicyBody.cost.insurancePremium,
                                PaymentDate = DateTime.Now.ToString("yyyy-MM-dd"),
                                StartDate = epolicyBody.details.startDate,
                                EndDate = epolicyBody.details.endDate
                            };

                            var payedPolicyDataBody = new StringContent(JsonConvert.SerializeObject(payedPolicy), Encoding.UTF8, "application/json");

                            var payedResponse = await _httpClient.PostAsync($"{_appConfig.FoundIntegrationApi}/epolicy/confirmpayed", payedPolicyDataBody);

                            payedResponseContent = response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<ApiResponse>(await payedResponse.Content.ReadAsStringAsync()) : null;

                            if (payedResponseContent != null)
                            {
                                if (payedResponseContent.Success)
                                {
                                    try
                                    {
                                        var payedPolicySeriaAndNumber = JsonConvert.DeserializeObject<ConfirmPayedResponce>(payedResponseContent.Data.ToString());

                                        policy.DocumentUID = policyUuid.uuid;
                                        policy.RegistrationSerial = payedPolicySeriaAndNumber.seria;
                                        policy.RegistrationNumber = payedPolicySeriaAndNumber.number;
                                        policy.State = CRM.Data.Enums.SalesPolicyState.Issued;
                                        policy.Deal.Stages = CRM.Data.Enums.SalesDealStages.Monitoring;
                                        _dbContext.Policies.Update(policy);
                                        await _dbContext.SaveChangesAsync();
                                    }
                                    catch (Exception ex)
                                    {

                                        throw (ex);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        payedResponseContent = responceContent;
                    }
                }

                return payedResponseContent;
            }
            else
            {
                //StreamReader r = new StreamReader(@"c:\\hujjatbody.json");
                //var jsonString = r.ReadToEnd();

                //var epolicyJson = JsonConvert.SerializeObject(jsonString);

                ////var epolicyDataBody = new StringContent(jsonString, Encoding.UTF8, "application/json");

                ////_messagePublisher.Publish(jsonString);

                //var epolicyJson = JsonConvert.SerializeObject(epolicy.Body); //working code
                                

                _messagePublisher.SendMessage("NEW_DOCUMENT_QUEUE", epolicy.Body);

                //_messagePublisher.SendMessage("NEW_DOCUMENT_QUEUE", jsonString);

                //r.Close();

                return new ApiResponse()
                {
                    Success = true,
                    Error = null
                };
            }
        }

    }
}