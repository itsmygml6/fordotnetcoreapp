﻿using CRM.Common;
using CRM.Common.Actions;
using CRM.Data.Core;
using CRM.Data.Enums;
using CRM.Data.Enums.Document;
using CRM.Data.Models;
using CRM.Data.Models.FrontOffice.Document.Entities.EPolis;
using CRM.Data.Models.FrontOffice.Document.Entities.HujjatUz;
using Frontoffice.DocumentRegistration.Handlers;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CRM.Data.Models.FrontOffice.Document.Entities.HujjatUz.DocumentParamsInner;
using Frontoffice.DocumentRegistration.DTO;

namespace Frontoffice.DocumentRegistration.Services
{
    public class PolicyRegistrationService
    {
        private readonly IAppDbContext _dbContext;
        private readonly AppConfig _appconfig;
        private readonly CounterAgentService _counterAgentService;
        private readonly HandBookService _handBookService;
        private readonly HttpClient _httpClient;
        private readonly SendPolicyService _policyService;
        private readonly ILogger<PolicyRegistrationService> _logger;
        private readonly IdentityService identityService;
        private readonly FoundService foundService;

        private readonly JsonSerializerSettings _jsonSettings = new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            Formatting = Formatting.None
        };

        public PolicyRegistrationService(IAppDbContext dbContext, AppConfig appConfig, CounterAgentService counterAgentService, HandBookService handBookService,
                                         HttpClient httpClient, SendPolicyService policyService, ILogger<PolicyRegistrationService> logger, IdentityService identityService, FoundService foundService)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _appconfig = appConfig;
            _counterAgentService = counterAgentService;
            _httpClient = httpClient;
            _handBookService = handBookService;
            _policyService = policyService;
            _logger = logger;
            this.identityService = identityService;
            this.foundService = foundService;
        }

        public async Task<ActionResult> RegisterPolicy(long policyId, string token)
        {
            var result = new ActionResult()
            {
                Status = ActionStatus.Success
            };

            if (policyId == 0)
            {
                result.Status = ActionStatus.Failed;
                result.Error = new ActionError()
                {
                    Message = "Incorrect policy id"
                };
                return result;
            }
            else
            {
                try
                {
                    //IGenericRepository<Policy> _policies = new GenericRepository<Policy>(_dbContext);

                    //var policy = await _policies.Get(p => p.Id == policyId, include: p => p.Include(p => p.Deal)
                    //    .ThenInclude(p => p.Application).ThenInclude(p => p.Participants)
                    //    .Include(p => p.Deal).ThenInclude(p => p.Seller).ThenInclude(p => p.Products));

                    var policy = await _dbContext.Policies.Where(p => p.Id == policyId)
                                   .Include(p => p.Deal)
                                   .ThenInclude(d => d.Application)
                                   .ThenInclude(a => a.Participants)
                                   .Include(p => p.Deal).ThenInclude(d => d.Application).ThenInclude(r=>r.Rates)
                                   .Include(p => p.Deal).ThenInclude(d => d.Application).ThenInclude(r => r.Objects).ThenInclude(o=>o.ObjectDetails)
                                   .Include(p => p.Deal).ThenInclude(s => s.Seller).ThenInclude(c => c.Products)
                                   .FirstOrDefaultAsync();



                    if (policy != null)
                    {
                        if (policy.Deal != null && policy.Deal.Application != null)
                        {
                            var productTemplate = await _dbContext.ProductTemplates.Where(p => p.Id == policy.Deal.Application.ProductTemplateId).FirstOrDefaultAsync();

                            if (productTemplate != null)
                            {
                                var newDocument = new Document();
                                var newDocumentBody = new DocumentBody();
                                if (productTemplate.TypeId == 17)//Code.Contains("OSGO"))
                                {
                                    newDocument.Type = LegalizationType.EPolis;
                                    var epolis = new EPolisDocumentBody();
                                    
                                    if (policy.Deal.Application != null && policy.Deal.Application.Participants != null)
                                    {
                                        if (policy.Deal.InsurantCounteragentId != null)//from deal Insurant
                                        {
                                            _httpClient.BaseAddress = new Uri(_appconfig.BaseUrlCounterAgent);
                                            epolis.applicant = new Applicant(); //Insurant from Deal
                                            var counterAgent = await _counterAgentService.GetCounteragentByIdAsync(policy.Deal.InsurantCounteragentId.Value, token);

                                            if (counterAgent != null)
                                            {
                                                if (counterAgent.Type == "Person")
                                                {
                                                    epolis.applicant.organization = null;
                                                    epolis.applicant.person = new ApplicantPerson(); // person = counteragent
                                                    var person = await _counterAgentService.GetPersonByIdAsync(policy.Deal.InsurantCounteragentId.Value, token);
                                                    if (person != null)
                                                    {
                                                        epolis.applicant.person.fullName.firstname = person.FirstName;
                                                        epolis.applicant.person.fullName.lastname = person.LastName;
                                                        epolis.applicant.person.fullName.middlename = person.FathersName;

                                                        epolis.applicant.person.passportData.pinfl = person.PINFL;
                                                        epolis.applicant.person.passportData.seria = person.DocumentSeries;
                                                        epolis.applicant.person.passportData.number = person.DocumentNumber;
                                                        epolis.applicant.person.passportData.issuedDate = person.DocumentGivenDate.ToString("yyyy-MM-dd");
                                                        epolis.applicant.person.passportData.issuedBy = person.GivenBy;

                                                        epolis.applicant.person.gender = (person.Gender == "Male") ? Gender.m.ToString() : Gender.f.ToString();
                                                        epolis.applicant.person.birthDate = person.BornDate.ToString("yyyy-MM-dd");
                                                        epolis.applicant.person.regionId = person.Addresses.FirstOrDefault(a => a.IsDefault).RegionId.ToString();
                                                        epolis.applicant.person.districtId = person.Addresses.FirstOrDefault(a => a.IsDefault).DistrictId.ToString();
                                                        var phoneVal = person.Contacts.FirstOrDefault(a => a.IsDefault && a.Type == "Phone");
                                                        if(phoneVal != null)
                                                        {
                                                            epolis.applicant.person.phoneNumber = phoneVal.Value.ToString().Replace("+", "");
                                                        }

                                                        epolis.applicant.address = person.Addresses.FirstOrDefault(a => a.IsDefault).Value;  // address = from counteragent
                                                        epolis.applicant.email = "example@mail.com";//person.Contacts?.FirstOrDefault(a => a.IsDefault && a.Type == "Email")?.Value;// email = from counteragent
                                                    }
                                                    else
                                                    {
                                                        result.Status = ActionStatus.Failed;
                                                        result.Error = new ActionError()
                                                        {
                                                            Message = "Insurant person not found"
                                                        };
                                                        return result;
                                                    }
                                                }
                                                else
                                                {
                                                    epolis.applicant.person = null;
                                                    epolis.applicant.organization = new ApplicantOrganization(); // organization = counteragent
                                                    var organization = await _counterAgentService.GetOrganisationByIdAsync(policy.Deal.InsurantCounteragentId.Value, token);
                                                    if (organization != null)
                                                    {
                                                        epolis.applicant.organization.inn = organization.TIN;
                                                        epolis.applicant.organization.name = organization.Title;
                                                        epolis.applicant.organization.regCertificateIssueDate = organization.RegistrationCertificateDate.ToString("yyyy-MM-dd");
                                                        //_httpClient.BaseAddress = new Uri(AppConst.BaseUrlHandBook);
                                                        //var bankId = organization.Requisites.FirstOrDefault(r => r.IsDefault).BankId; //epolis.applicant.organization.bankMfo = ;
                                                        //if (bankId != null)
                                                        //{
                                                        //    var bank = await _handBookService.GetBankByIdAsync(_httpClient, bankId.Value);
                                                        //    if (bank != null)
                                                        //    {
                                                        //        epolis.applicant.organization.bankMfo = bank.MFO;
                                                        //    }
                                                        //    else
                                                        //    {
                                                        //        result.Status = ActionStatus.Failed;
                                                        //        result.Error = new ActionError()
                                                        //        {
                                                        //            Message = "Organization bank not exist in system"
                                                        //        };
                                                        //        return result;
                                                        //    }
                                                        //}
                                                        //else
                                                        //{
                                                        //    result.Status = ActionStatus.Failed;
                                                        //    result.Error = new ActionError()
                                                        //    {
                                                        //        Message = "Organization bank not found"
                                                        //    };
                                                        //    return result;
                                                        //}
                                                        var appAdrVal = organization.Addresses.FirstOrDefault(a => a.IsDefault);
                                                        if (appAdrVal != null)
                                                        {
                                                            epolis.applicant.address = appAdrVal.Value;
                                                        }
                                                        //epolis.applicant.address = organization.Addresses.FirstOrDefault(a => a.IsDefault).Value;  // address = from counteragent
                                                        var appEmailVal = organization.Contacts.FirstOrDefault(a => a.IsDefault && a.Type == "Email");
                                                        if (appEmailVal != null)
                                                        {
                                                            epolis.applicant.email = appEmailVal.Value;
                                                        }
                                                        //epolis.applicant.email = organization.Contacts.FirstOrDefault(a => a.IsDefault && a.Type == "Email").Value;// email = from counteragent
                                                    }
                                                    else
                                                    {
                                                        result.Status = ActionStatus.Failed;
                                                        result.Error = new ActionError()
                                                        {
                                                            Message = "Insurant organization not found"
                                                        };
                                                        return result;
                                                    }


                                                }
                                            }
                                            else
                                            {
                                                result.Status = ActionStatus.Failed;
                                                result.Error = new ActionError()
                                                {
                                                    Message = "Insurant counter agent not found"
                                                };
                                                return result;
                                            }
                                        }
                                        else
                                        {
                                            result.Status = ActionStatus.Failed;
                                            result.Error = new ActionError()
                                            {
                                                Message = "Policy application insurant not found"
                                            };
                                            return result;
                                        }

                                        ApplicationParticipant owner = policy.Deal.Application.Participants.FirstOrDefault(p => p.Role == RoleType.PropertyOwner);
                                        epolis.owner = new Owner(); //Owner
                                        if (owner != null && owner.CounteragentId != policy.Deal.InsurantCounteragentId.Value)
                                        {
                                            _httpClient.BaseAddress = new Uri(_appconfig.BaseUrlCounterAgent);
                                            epolis.owner.applicantIsOwner = false;
                                            var counterAgent = await _counterAgentService.GetCounteragentByIdAsync(owner.CounteragentId, token);
                                            if (counterAgent != null)
                                            {
                                                if (counterAgent.Type == "Person")
                                                {
                                                    epolis.owner.person = new OwnerPerson(); // person = counteragent
                                                    var person = await _counterAgentService.GetPersonByIdAsync(owner.CounteragentId, token);
                                                    if (person != null)
                                                    {
                                                        epolis.owner.person.fullName.firstname = person.FirstName;
                                                        epolis.owner.person.fullName.lastname = person.LastName;
                                                        epolis.owner.person.fullName.middlename = person.FathersName;

                                                        epolis.owner.person.passportData.pinfl = person.PINFL;
                                                        epolis.owner.person.passportData.seria = person.DocumentSeries;
                                                        epolis.owner.person.passportData.number = person.DocumentNumber;
                                                        epolis.owner.person.passportData.issuedDate = person.DocumentGivenDate.ToString("yyyy-MM-dd");
                                                        epolis.owner.person.passportData.issuedBy = person.GivenBy;
                                                    }
                                                    else
                                                    {
                                                        result.Status = ActionStatus.Failed;
                                                        result.Error = new ActionError()
                                                        {
                                                            Message = "Insurant person not found"
                                                        };
                                                        return result;
                                                    }
                                                }
                                                else
                                                {
                                                    // organization = participant get ParticipantRecord type and if participant is Organization then set organization data
                                                    epolis.owner.organization = new OwnerOrganization(); // organization = counteragent
                                                    var organization = await _counterAgentService.GetOrganisationByIdAsync(owner.CounteragentId, token);
                                                    if (organization != null)
                                                    {
                                                        epolis.owner.organization.inn = organization.TIN;
                                                        epolis.owner.organization.name = organization.Title;
                                                    }
                                                    else
                                                    {
                                                        result.Status = ActionStatus.Failed;
                                                        result.Error = new ActionError()
                                                        {
                                                            Message = "Insurant organization not found"
                                                        };
                                                        return result;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                result.Status = ActionStatus.Failed;
                                                result.Error = new ActionError()
                                                {
                                                    Message = "Insurant counter agent not found"
                                                };
                                                return result;
                                            }
                                        }
                                        else
                                        {
                                            epolis.owner.person = null;
                                            epolis.owner.organization = null;
                                            epolis.owner.applicantIsOwner = true;// applicantIsOwner = if (ApplicantPerson/ApplicantOrganization is equal to OwnerPerson/OwnerOrganization set true and person and organization fields not fiiled)
                                                                                   //result.Status = ActionStatus.Failed;
                                                                                   //result.Error = new ActionError()
                                                                                   //{
                                                                                   //    Message = "Policy application InsuredPerson not found"
                                                                                   //};
                                                                                   //return result;
                                        }

                                        epolis.details = new Details(); // policy, application
                                        epolis.details.issueDate = policy.Deal.Application.CreatedDate.ToString("yyyy-MM-dd"); // issuedDate = policy issue date
                                        epolis.details.startDate = policy.Deal.Application.InsuranceStart.ToString("yyyy-MM-dd"); // startDate = policy start date
                                        epolis.details.endDate = policy.Deal.Application.InsuranceEnd.ToString("yyyy-MM-dd"); // endDate = policy end date
                                        var rate = policy.Deal.Application?.Rates?.FirstOrDefault(r => r.Title == " - не ограничено");
                                        if (rate != null)
                                        {
                                            epolis.details.driverNumberRestriction = false;
                                        }
                                        else
                                        {
                                            epolis.details.driverNumberRestriction = true;// driverNumberRestriction = find in application rates with name which equal to "driverNumberRestriction" and Premiuim is more than 0
                                        }
                                        epolis.details.specialNote = string.Empty;// specialNote = empty
                                        epolis.details.insuredActivityType = string.Empty;// insuredActivityType = find in application rates with name which equal to "insuredActivityType" and Premiuim is more than 0

                                        epolis.cost = new Cost(); // application
                                        epolis.cost.discountId = "1";// discountId = /api/references/discount
                                        epolis.cost.discountSum = "0";// discountSum = 0 ???
                                        epolis.cost.insurancePremium = policy.Deal.Application.InsurancePremium.ToString();// insurancePremium = application.insurancePremium
                                        epolis.cost.sumInsured = policy.Deal.Application.InsuranceSum.ToString();// sumInsured = application.insuranceSum
                                        epolis.cost.contractTermConclusionId = "1";                              // contractTermConclusionId = /api/references/contract-term-conclusions 1year
                                        var territoryRate = policy.Deal.Application?.Rates?.FirstOrDefault(r => r.Title == " - г. Ташкент и Ташкентская область");
                                        if (territoryRate != null )//&& (territoryRate.Premium == 1.4m))// useTerritoryId = /api/references/use-territory-region
                                        {
                                            epolis.cost.useTerritoryId = "1";
                                        }
                                        else
                                        {
                                            epolis.cost.useTerritoryId = "2";
                                        }
                                        var productComission = policy.Deal.Seller.Products.FirstOrDefault(s => s.ProductId == policy.Deal.Application.ProductTemplateId);//epolis.cost.commission = policy.Deal.Application.InsurancePremium - policy.Deal.Seller.Product
                                                                                                                                                                         // commission = application.InsurancePremium - Deal.Seller.SellerProduct.Commission  ???
                                        if (productComission != null)
                                        {
                                            epolis.cost.commission = productComission.Commission.ToString();
                                        }
                                        else
                                        {
                                            epolis.cost.commission = "0";
                                        }
                                        epolis.cost.insurancePremiumPaidToInsurer = (policy.Deal.Application.InsuranceSum - productComission.Commission).ToString();// insurancePremiumPaidToInsurer = application.InsurancePremium - commission
                                        epolis.cost.seasonalInsuranceId = "1"; // seasonalInsuranceId = /api/references/seasonal-insurances 1year

                                        epolis.vehicle = new Vehicle(); // application.objects.objectdetail.details
                                        epolis.vehicle.techPassport = new TechPassport();
                                        var viechle = policy.Deal.Application.Objects.FirstOrDefault(o => true);
                                        if (viechle != null)
                                        {
                                            var pIssueDate = viechle.ObjectDetails.FirstOrDefault(v => v.Title == "techPassport.seria");//"techPassport.issueDate");n
                                            if (pIssueDate != null)
                                            {
                                                epolis.vehicle.techPassport.issueDate = DateTime.Parse(pIssueDate.Details).ToString("yyyy-MM-dd"); 
                                            }

                                            var pSeria = viechle.ObjectDetails.FirstOrDefault(v => v.Title == "techPassport.number");//"techPassport.seria");d
                                            if (pSeria != null)
                                            {
                                                epolis.vehicle.techPassport.seria = pSeria.Details;
                                            }

                                            var pNumber = viechle.ObjectDetails.FirstOrDefault(v => v.Title == "techPassport.issueDate");//"techPassport.number");s
                                            if (pNumber != null)
                                            {
                                                epolis.vehicle.techPassport.number = pNumber.Details;
                                            }

                                            var engineNumber = viechle.ObjectDetails.FirstOrDefault(v => v.Title == "engineNumber");
                                            if (engineNumber != null)
                                            {
                                                epolis.vehicle.engineNumber = engineNumber.Details;
                                            }

                                            var modelCustomName = viechle.ObjectDetails.FirstOrDefault(v => v.Title == "modelCustomName");
                                            if (modelCustomName != null)
                                            {
                                                epolis.vehicle.modelCustomName = modelCustomName.Details;
                                            }

                                            var typeId = viechle.ObjectDetails.FirstOrDefault(v => v.Title == "typeId");
                                            if (typeId != null)
                                            {
                                                epolis.vehicle.typeId = typeId.Details;
                                            }

                                            var bodyNumber = viechle.ObjectDetails.FirstOrDefault(v => v.Title == "bodyNumber");
                                            if (bodyNumber != null)
                                            {
                                                epolis.vehicle.bodyNumber = bodyNumber.Details;
                                            }

                                            var govNumber = viechle.ObjectDetails.FirstOrDefault(v => v.Title == "govNumber");
                                            if (govNumber != null)
                                            {
                                                epolis.vehicle.govNumber = govNumber.Details;
                                            }

                                            var issueYear = viechle.ObjectDetails.FirstOrDefault(v => v.Title == "issueYear");
                                            if (issueYear != null)
                                            {
                                                epolis.vehicle.issueYear = issueYear.Details;
                                            }

                                            var regionId = viechle.ObjectDetails.FirstOrDefault(v => v.Title == "regionId");
                                            if (regionId != null)
                                            {
                                                epolis.vehicle.regionId = regionId.Details;
                                            }
                                        }
                                        else
                                        {
                                            result.Status = ActionStatus.Failed;
                                            result.Error = new ActionError()
                                            {
                                                Message = "Viechle not exist"
                                            };
                                            return result;
                                        }

                                        // techPassport = application.objects.find first, then find in List of ObjectDetail where Title is equal to techPassport value and set TechPassport with ObjectDetail.Details
                                        // modelCustomName = application.objects.find first, then find in List of ObjectDetail where Title is equal to modelCustomName value and set ModelCustomName with ObjectDetail.Details
                                        // engineNumber = application.objects.find first, then find in List of ObjectDetail where Title is equal to engineNumber value and set engineNumber with ObjectDetail.Details 
                                        // typeId = application.objects find first, then find in List of ObjectDetail where Title is qual to typeId value and set typeId  with ObjectDetail.Details, this detail should fill from /api/references/vehicle-types
                                        // issueYear = - || -
                                        // govNumber = - || -
                                        // bodyNumber = - || -
                                        // regionId = - || -
                                        epolis.drivers = new List<Drivers>();  // application.objects.objectdetail.details
                                        if (epolis.details.driverNumberRestriction.Value)
                                        {
                                            foreach (var item in policy.Deal.Application.Participants)
                                            {
                                                if (item.Role == RoleType.Principal)
                                                {
                                                    var driver = new Drivers();
                                                    var counterAgent = await _counterAgentService.GetPersonByIdAsync(item.CounteragentId, token);
                                                    if (counterAgent != null)
                                                    {
                                                        driver.passportData = new PassportData();// PassportData = - || -
                                                        driver.passportData.issuedDate = counterAgent.DocumentGivenDate.ToString("yyyy-MM-dd");
                                                        driver.passportData.issuedBy = counterAgent.GivenBy;
                                                        driver.passportData.number = counterAgent.DocumentNumber;
                                                        driver.passportData.seria = counterAgent.DocumentSeries;
                                                        driver.passportData.pinfl = counterAgent.PINFL;
                                                        driver.fullName = new FullName();// fullName = - || -
                                                        driver.fullName.firstname = counterAgent.FirstName;
                                                        driver.fullName.lastname = counterAgent.FathersName;
                                                        driver.fullName.middlename = counterAgent.LastName;
                                                        driver.birthDate = counterAgent.BornDate.ToString("yyyy-MM-dd");// birthDate = - || -
                                                        var perPinfl = new PersonByPinflRequestDTO
                                                        {
                                                            passportSeries = counterAgent.DocumentSeries,
                                                            passportNumber = counterAgent.DocumentNumber,
                                                            pinfl = counterAgent.PINFL
                                                        };
                                                        var drLicResult = await foundService.GetDriverLicense(perPinfl);
                                                        driver.licenseIssueDate = drLicResult.IssueDate.ToString("yyyy-MM-dd"); // licenseIssueDate = - || -
                                                        driver.licenseSeria = drLicResult.LicenseSeria;
                                                        driver.licenseNumber = drLicResult.LicenseNumber;// licenseNumber = - || -/*
                                                        //drLicResult.
                                                        //driver.licenseIssueDate = item.licenseIssueDate.Value.ToString("yyyy-MM-dd"); // licenseIssueDate = - || -
                                                        //driver.licenseSeria = item.licenseSeria;
                                                        //driver.licenseNumber = item.licenseNumber;// licenseNumber = - || -/*
                                                        driver.relative = 0;// relative = - || -
                                                        driver.residentOfUzb = 1;// residentOdUzb = - || -

                                                        epolis.drivers.Add(driver);
                                                    }
                                                }
                                            }
                                        }

                                        newDocumentBody.Body = JsonConvert.SerializeObject(epolis);
                                        //newDocumentBody.RequestId = epolis.RequestId;
                                    }
                                    else
                                    {
                                        result.Status = ActionStatus.Failed;
                                        result.Error = new ActionError()
                                        {
                                            Message = "Policy application error"
                                        };
                                        return result;
                                    }
                                }
                                else
                                {
                                    var hujjatPattern = new HujjatUzPattern();
                                    hujjatPattern.Pattern = "NEW_DOCUMENT_QUEUE";
                                    
                                    var epolis = new HujjatUzDocumentBody();
                                    hujjatPattern.Body = epolis;

                                    newDocument.TemplateUID = "7f61f5e7-0a9a-4901-9eca-d9c689135ad0";
                                    newDocument.Type = LegalizationType.HujjatUz;
                                    epolis.RequestId = Guid.NewGuid().ToString();
                                    epolis.Attachments = null;
                                    epolis.Attributes = new Attributes
                                    {
                                        Mode = _appconfig.Environment
                                    };
                                    epolis.DocumentType = DocumentType.FTL_JSON.ToString();
                                    epolis.DocumentParams = new DocumentParams();
                                    epolis.DocumentParams.TemplateId = newDocument.TemplateUID;
                                    epolis.DocumentParams.Lang = Language.UZ.ToString().ToLower();

                                    var documentParamsModel = new Model();

                                    documentParamsModel.serialNumber = policy.RegistrationSerial + " " + policy.RegistrationNumber;

                                    var person = await _counterAgentService.GetPersonByIdAsync(policy.Deal.InsurantCounteragentId.Value, token);

                                    var insurer = new Insurer();
                                    
                                    insurer.fullName = person.FirstName + " "+ person.LastName + " " + person.FathersName;
                                    var phoneNumber = person.Contacts.FirstOrDefault(a => a.Type == "Phone");
                                    if (phoneNumber != null)
                                    {
                                        insurer.phoneNumber = phoneNumber.Value.ToString().Replace("+", "");
                                    }
                                    
                                    insurer.id = policy.Deal.InsurantCounteragentId.ToString();

                                    documentParamsModel.insurer = insurer;
                                    documentParamsModel.traveler1 = new Traveler1()
                                    {
                                        birthDate = "",
                                        condition = "",
                                        fullName = "",
                                        passportSerial = ""
                                    };
                                    documentParamsModel.traveler2 = new Traveler2()
                                    {
                                        birthDate = "",
                                        condition = "",
                                        fullName = "",
                                        passportSerial = ""
                                    };
                                    documentParamsModel.traveler3 = new Traveler3()
                                    {
                                        birthDate = "",
                                        condition = "",
                                        fullName = "",
                                        passportSerial = ""
                                    };
                                    documentParamsModel.traveler4 = new Traveler4()
                                    {
                                        birthDate = "",
                                        condition = "",
                                        fullName = "",
                                        passportSerial = ""
                                    };
                                    documentParamsModel.traveler5 = new Traveler5()
                                    {
                                        birthDate = "",
                                        condition = "",
                                        fullName = "",
                                        passportSerial = ""
                                    };
                                    documentParamsModel.traveler6 = new Traveler6()
                                    {
                                        birthDate = "",
                                        condition = "",
                                        fullName = "",
                                        passportSerial = ""
                                    };
                                    int indexCounter = 0;
                                    foreach (var item in policy.Deal.Application.Participants.Select((value, index) => new { index, value }))
                                    {

                                        if (item.value.Role == RoleType.InsuredPerson || (policy.Deal.Application.Participants.Count == 1))
                                        {
                                            switch (indexCounter)
                                            {
                                                case 0:
                                                    {
                                                        var traveler1 = new Traveler1();
                                                        var counterAgent = await _counterAgentService.GetPersonByIdAsync(item.value.CounteragentId, token);
                                                        if (counterAgent != null)
                                                        {
                                                            traveler1.fullName = counterAgent.LastName + " " + counterAgent.FirstName + " " + counterAgent.FathersName;
                                                            traveler1.passportSerial =counterAgent.DocumentSeries + " " + counterAgent.DocumentNumber;
                                                            traveler1.birthDate = counterAgent.BornDate.ToString("dd.MM.yyyy");                                                            
                                                            traveler1.condition = "";
                                                        }
                                                        documentParamsModel.traveler1 = traveler1;
                                                        break;
                                                    }
                                                case 1:
                                                    {
                                                        var traveler2 = new Traveler2();
                                                        var counterAgent = await _counterAgentService.GetPersonByIdAsync(item.value.CounteragentId, token);
                                                        if (counterAgent != null)
                                                        {
                                                            traveler2.fullName = counterAgent.LastName + " " + counterAgent.FirstName + " " + counterAgent.FathersName;
                                                            traveler2.passportSerial =counterAgent.DocumentSeries + " " + counterAgent.DocumentNumber;
                                                            traveler2.birthDate = counterAgent.BornDate.ToString("dd.MM.yyyy");                                                            
                                                            traveler2.condition = "";
                                                        }
                                                        documentParamsModel.traveler2 = traveler2;
                                                        break;
                                                    }
                                                case 2:
                                                    {
                                                        var traveler3 = new Traveler3();
                                                        var counterAgent = await _counterAgentService.GetPersonByIdAsync(item.value.CounteragentId, token);
                                                        if (counterAgent != null)
                                                        {
                                                            traveler3.fullName = counterAgent.LastName + " " + counterAgent.FirstName + " " + counterAgent.FathersName;
                                                            traveler3.passportSerial = counterAgent.DocumentSeries + " " + counterAgent.DocumentNumber;
                                                            traveler3.birthDate = counterAgent.BornDate.ToString("dd.MM.yyyy");                                                            
                                                            traveler3.condition = "";
                                                        }
                                                        documentParamsModel.traveler3 = traveler3;
                                                        break;
                                                    }
                                                case 3:
                                                    {
                                                        var traveler4 = new Traveler4();
                                                        var counterAgent = await _counterAgentService.GetPersonByIdAsync(item.value.CounteragentId, token);
                                                        if (counterAgent != null)
                                                        {
                                                            traveler4.fullName = counterAgent.LastName + " " + counterAgent.FirstName + " " + counterAgent.FathersName;
                                                            traveler4.passportSerial = counterAgent.DocumentSeries + " " + counterAgent.DocumentNumber;
                                                            traveler4.birthDate = counterAgent.BornDate.ToString("dd.MM.yyyy");                                                            
                                                            traveler4.condition = "";                                                            
                                                        }
                                                        documentParamsModel.traveler4 = traveler4;
                                                        break;
                                                    }
                                                case 4:
                                                    {
                                                        var traveler5 = new Traveler5();
                                                        var counterAgent = await _counterAgentService.GetPersonByIdAsync(item.value.CounteragentId, token);
                                                        if (counterAgent != null)
                                                        {
                                                            traveler5.fullName = counterAgent.LastName + " " + counterAgent.FirstName + " " + counterAgent.FathersName;
                                                            traveler5.passportSerial = counterAgent.DocumentSeries + " " + counterAgent.DocumentNumber;
                                                            traveler5.birthDate = counterAgent.BornDate.ToString("dd.MM.yyyy");                                                            
                                                            traveler5.condition = "";                                                            
                                                        }
                                                        documentParamsModel.traveler5 = traveler5;
                                                        break;
                                                    }
                                                case 5:
                                                    {
                                                        var traveler6 = new Traveler6();
                                                        var counterAgent = await _counterAgentService.GetPersonByIdAsync(item.value.CounteragentId, token);
                                                        if (counterAgent != null)
                                                        {
                                                            traveler6.fullName = counterAgent.LastName + " " + counterAgent.FirstName + " " + counterAgent.FathersName;
                                                            traveler6.passportSerial = counterAgent.DocumentSeries + " " + counterAgent.DocumentNumber;
                                                            traveler6.birthDate = counterAgent.BornDate.ToString("dd.MM.yyyy");                                                            
                                                            traveler6.condition = "";                                                            
                                                        }
                                                        documentParamsModel.traveler6 = traveler6;
                                                        break;
                                                    }
                                            }
                                            indexCounter++;
                                        }
                                    }

                                    documentParamsModel.program = (await _dbContext.ProductTemplates.FirstOrDefaultAsync(p => p.Id == policy.Deal.Application.ProductTemplateId))?.Title;
                                    
                                    documentParamsModel.riskAmount = policy.Deal.Application.InsuranceSum;
                                    documentParamsModel.riskAmountCurrency = "EURO";
                                    documentParamsModel.amount = policy.Deal.Application.InsurancePremium;
                                    documentParamsModel.amountCurrency = "UZS";
                                    documentParamsModel.dateOfIssue = policy.Deal.Application.InsuranceStart.ToString("dd.MM.yyyy");
                                    documentParamsModel.countries = policy.Deal.Application.ExtraData;//"France,Schengen";
                                    documentParamsModel.travelDays = (policy.EndDate - policy.StartDate).Days;
                                    documentParamsModel.endDate = policy.EndDate.ToString("dd.MM.yyyy");
                                    documentParamsModel.fromDate = policy.StartDate.ToString("dd.MM.yyyy");
                                    epolis.OldId = null;
                                    epolis.OldPin = null;

                                    epolis.DocumentParams.Model = documentParamsModel;

                                    epolis.DocumentParams.SignTemplateUrls = new SignTemplateUrls
                                    {
                                        Authorization = null,
                                        Cancel = null,
                                        Create = null,
                                        Reject = null,
                                    };
                                    epolis.SignType = SignType.EIMZO.ToString();
                                    
                                    epolis.SignParams = new SignParams();
                                    epolis.SignParams.AllowLevel = AllowLevel.LEVEL.ToString();

                                    string tmpStrAllows = "{\"0\":[\"552068934\"]}";
                                    string tmpTagAllows = "\"#REPLACEALLOW\"";
                                    epolis.SignParams.Allows = "#REPLACEALLOW";
                                    
                                    string tmpStr = "{\"0\": [\"998974030711\"]}";
                                    string tmpTag = "\"#REPLACE\"";
                                    epolis.Notice = "#REPLACE";

                                    newDocumentBody.Body = JsonConvert.SerializeObject(hujjatPattern.Body);
                                    newDocumentBody.Body = newDocumentBody.Body.Replace(tmpTagAllows, tmpStrAllows);
                                    newDocumentBody.Body = newDocumentBody.Body.Replace(tmpTag, tmpStr);                                    
                                    newDocumentBody.RequestId = epolis.RequestId;

                                }

                                newDocumentBody.PolicyId = policyId;
                                newDocument.DocumentBody = newDocumentBody;
                                newDocumentBody.Document = newDocument;

                                var response = await _policyService.SendToPolicyService(newDocumentBody);
                                if (response != null && response.Success)
                                {
                                    newDocument.State = DocumentState.Pending;
                                }
                                else
                                {
                                    newDocument.State = DocumentState.Failed;
                                }
                                _dbContext.Documents.Add(newDocument);
                                await _dbContext.SaveChangesAsync();

                                _dbContext.Policies.Update(policy);
                                await _dbContext.SaveChangesAsync();
                            }
                            else
                            {
                                result.Status = ActionStatus.Failed;
                                result.Error = new ActionError()
                                {
                                    Message = "Product template not found"
                                };
                            }
                        }
                        else
                        {
                            result.Status = ActionStatus.Failed;
                            result.Error = new ActionError()
                            {
                                Message = "Deal or Application not found"
                            };
                        }
                    }
                    else
                    {
                        result.Status = ActionStatus.Failed;
                        result.Error = new ActionError()
                        {
                            Message = "Policy not found"
                        };
                    }
                }
                catch (Exception exception)
                {



                    var message = exception.InnerException != null ? exception.InnerException.Message : exception.Message;

                    result.Status = ActionStatus.Failed;
                    result.Error = new ActionError()
                    {
                        Message = message
                    };
                }
            }


            return result;
        }

        public async Task<ActionResult> UpdateDocumentState(SignDocumentData document)
        {
            var result = new ActionResult
            {
                Error = new ActionError
                {
                    Code = 0,
                    Message = ""
                },
                Status = ActionStatus.Success,
            };
            var token = await identityService.GetAccessToken();
            try
            {
                var doc = await _dbContext.Documents.Where(d => d.DocumentBody.RequestId == document.requestId)
                    .Include(b => b.DocumentBody).ThenInclude(p => p.Policy).ThenInclude(d=>d.Deal)
                    .FirstOrDefaultAsync();


                if (document.state == "CANCELED")
                {
                    if (doc != null)
                    {
                        doc.State = DocumentState.Rejected;
                        if (doc.DocumentBody != null)
                        {
                            doc.DocumentBody.Policy.State = SalesPolicyState.Issued;
                            if (doc.DocumentBody.Policy.Deal != null)
                            {
                                doc.DocumentBody.Policy.Deal.Stages = SalesDealStages.Closing;
                                var person = await _counterAgentService.GetPersonByIdAsync(doc.DocumentBody.Policy.Deal.InsurantCounteragentId.Value, token);

                                var phoneNumber = person.Contacts.FirstOrDefault(a => a.Type == "Phone");
                                if (phoneNumber != null)
                                {
                                    SmsService smsService = new SmsService();
                                    await smsService.SendSmsAsync(phoneNumber.Value.ToString(), $"Vash polis otmenen: {doc.DocumentUrl}");
                                }
                            }
                        }
                        await _dbContext.SaveChangesAsync();
                    }
                }
                else if (document.state == "SIGNED")
                {
                    if (doc != null)
                    {
                        doc.State = DocumentState.Signed;
                        if (doc.DocumentBody != null)
                        {
                            doc.DocumentBody.Policy.State = SalesPolicyState.Signed;
                            if (doc.DocumentBody.Policy.Deal != null)
                            {
                                doc.DocumentBody.Policy.Deal.Stages = SalesDealStages.Monitoring;
                                var person = await _counterAgentService.GetPersonByIdAsync(doc.DocumentBody.Policy.Deal.InsurantCounteragentId.Value, token);

                                var phoneNumber = person.Contacts.FirstOrDefault(a => a.Type == "Phone");
                                if (phoneNumber != null)
                                {
                                    SmsService smsService = new SmsService();
                                    await smsService.SendSmsAsync(phoneNumber.Value.ToString(), $"Vash polis podtverzhden: {doc.DocumentUrl}");
                                }
                            }
                        }
                        
                        await _dbContext.SaveChangesAsync();
                    }
                }
                else
                {
                    if (doc != null)
                    {
                        doc.State = DocumentState.Failed;
                        if (doc.DocumentBody != null)
                        {
                            doc.DocumentBody.Policy.State = SalesPolicyState.Created;
                        }
                        await _dbContext.SaveChangesAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                result.Error.Message = ex.Message;
                result.Status = ActionStatus.Failed;
            }
            return result;
        }

        public async Task<ActionResult> UpdateDocumentState(UpdateDocumentData document)
        {
            var result = new ActionResult
            {
                Error = new ActionError
                {
                    Code = 0,
                    Message = ""
                },
                Status = ActionStatus.Success,
            };
            var token = await identityService.GetAccessToken();
            if (document.requestId == null && document.error != null)
            {
                result.Status = ActionStatus.Failed;
                result.Error.Message = $"{document.error}:{document.message}";
                return result;
            }
            var doc = await _dbContext.Documents.Where(d => d.DocumentBody.RequestId == document.requestId)
                .Include(b => b.DocumentBody).ThenInclude(p => p.Policy).ThenInclude(d=>d.Deal)
                .FirstOrDefaultAsync();

            if (doc != null)
            {
                doc.DocumentUrl = document.documentUrl;
                doc.DocumentPin = document.pin;
                doc.DocumentUID = document.id;
                doc.State = DocumentState.Sent;
                doc.DocumentBody.Policy.DocumentUID = document.id;

                var person = await _counterAgentService.GetPersonByIdAsync(doc.DocumentBody.Policy.Deal.InsurantCounteragentId.Value, token);

                var phoneNumber = person.Contacts.FirstOrDefault(a => a.Type == "Phone");
                if (phoneNumber != null)
                {
                    SmsService smsService = new SmsService();
                    await smsService.SendSmsAsync(phoneNumber.Value.ToString(), "Polis zaregistrirovan.Ozhidayte soobshcheniye s linkom na dokument");
                }
                //if (doc.DocumentBody != null)
                //{
                //    doc.DocumentBody.Policy.State = SalesPolicyState.Issued;
                //}
                await _dbContext.SaveChangesAsync();
            }
            return result;
        }
    }
}
