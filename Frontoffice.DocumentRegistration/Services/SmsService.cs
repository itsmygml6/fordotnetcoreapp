﻿using CRM.Common.Rest;
using Frontoffice.DocumentRegistration.DTO;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;

namespace Frontoffice.DocumentRegistration.Services
{
    public class SmsService 
    {
        string PLAYMOBILE_URL = "http://91.204.239.44/broker-api/";
        string PLAYMOBILE_USER = "apexinsurance";
        string PLAYMOBILE_PASSWORD = "PJ$uOiY8K";
        HttpClient client;
        public SmsService(string baseUrl = "") 
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(PLAYMOBILE_URL);
            var byteArray = Encoding.ASCII.GetBytes($"{PLAYMOBILE_USER}:{PLAYMOBILE_PASSWORD}");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ApiResponseType.JsonResponse));

        }

        public async Task<string> SendSmsAsync(string recipient, string message)
        {
            var smsMessages = new SmsMessages
            {
                Messages = new List<SmsMessage>()
                {
                    new SmsMessage()
                    {
                        MessageId = $"apx"+DateTime.Now.ToString("yyMMddHHmmss"),
                        Recipient = recipient,
                        Sms = new SmsDto
                        {
                            Originator = "3700",
                            Content = new ContentSms
                            {
                                Text = message
                            }
                        }
                    }
                    
                }
                
                 
            };

            var smsContent = JsonConvert.SerializeObject(smsMessages);
            
            var requestContent = new StringContent(smsContent, Encoding.UTF8, "application/json");
            var response = await client.PostAsync("send", requestContent);

            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();

                return result;

            }

            throw new Exception(response.ReasonPhrase);
            

        }


    }
}
