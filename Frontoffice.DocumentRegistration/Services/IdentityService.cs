﻿using CRM.Common.Rest;
using Frontoffice.DocumentRegistration.DTO;

namespace Frontoffice.DocumentRegistration.Services
{
    public class IdentityService : ApiClient
    {
        private readonly string _clientId;
        private readonly string _clientSecret;

        public IdentityService(string baseUrl, string clientId, string clientSecret) : base(baseUrl)
        {
            _clientId = clientId;
            _clientSecret = clientSecret;
        }

        public async Task<string> GetAccessToken()
        {
            var result = await Get<UserTokenDto>($"Account/Auth?client={_clientId}&secret={_clientSecret}");

            if (result == null)
                throw new Exception("Token not found");


            return result.AccessToken;
        }
    }
}
