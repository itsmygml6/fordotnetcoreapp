﻿using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using Newtonsoft.Json;
using CRM.Common;
using Frontoffice.RabbitMQ.Core;
using Frontoffice.DocumentRegistration.Handlers;

namespace Frontoffice.DocumentRegistration.Services
{
    public class UpdateDocumentProcessingService : IHostedService
    {
        private readonly IModel _channel;
        private readonly ILogger<UpdateDocumentProcessingService> _logger;
        private PolicyRegistrationService _policyService;
        private readonly IServiceProvider _services;

        public UpdateDocumentProcessingService(AppConfig config, ILogger<UpdateDocumentProcessingService> logger, RabbitClient client, IServiceProvider services)
        {
            _logger = logger;
            _channel = client.CreateChannel(config.RabbitMQConnection, config.RabbitMQUser, config.RabbitMQPassword);
            _services = services;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            var scope = _services.CreateScope();
            _policyService = scope.ServiceProvider.GetRequiredService<PolicyRegistrationService>();
            //           _channel.ExchangeDeclare(RabbitKeys.UPDATE_DOCUMENT_QUEUE, ExchangeType.Direct);

            //var queueName = _channel.QueueDeclare(RabbitKeys.UPDATE_DOCUMENT_QUEUE);


            _channel.ExchangeDeclare(RabbitKeys.UPDATE_DOCUMENT_QUEUE, ExchangeType.Direct);
            _channel.QueueDeclare(RabbitKeys.UPDATE_DOCUMENT_QUEUE, true, false, false);
            


            _channel.QueueBind(RabbitKeys.UPDATE_DOCUMENT_QUEUE, RabbitKeys.UPDATE_DOCUMENT_QUEUE, "");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (sender, e) =>
            {
                await Task.Factory.StartNew(async (body) =>
                {
                    var message = Encoding.UTF8.GetString((byte[])body);
                    //var commandRequest = JsonConvert.DeserializeObject<UpdateDocumentData>(message);

                    //if (commandRequest != null)
                    //{
                    //    await _policyService.UpdateDocumentState(commandRequest);
                    //}

                    var commandRequest = JsonConvert.DeserializeObject<DocumentExchangeContainer<UpdateDocumentData>>(message);

                    var commandRequest2 = JsonConvert.DeserializeObject<UpdateDocumentData>(message);

                    if (commandRequest.data != null)
                    {
                        await _policyService.UpdateDocumentState(commandRequest.data);
                    }
                    else
                    {
                        await _policyService.UpdateDocumentState(commandRequest2);
                    }

                }, e.Body.ToArray(), cancellationToken);
            };

            _channel.BasicConsume(RabbitKeys.UPDATE_DOCUMENT_QUEUE, true, consumer);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _channel.Close();

            return Task.CompletedTask;
        }
    }
}
