﻿namespace Frontoffice.DocumentRegistration.Services.RabbitMQ.Producer
{
    public interface IMessageProducer
    {
        void SendMessage (string canelName, string message);
    }
}
