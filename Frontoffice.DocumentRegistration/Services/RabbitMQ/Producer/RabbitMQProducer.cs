﻿#nullable disable
using CRM.Common;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;

namespace Frontoffice.DocumentRegistration.Services.RabbitMQ.Producer
{
    public class RabbitMQProducer : IMessageProducer
    {
        private readonly AppConfig _appConfig;
        
        public RabbitMQProducer(AppConfig appConfig)
        {
            _appConfig = appConfig;
        }

        public void SendMessage(string channelName, string message)
        {
            var factory = new ConnectionFactory
            {
                HostName = _appConfig.RabbitMQConnection,
                Password = _appConfig.RabbitMQPassword,
                UserName = _appConfig.RabbitMQUser,
                Port = 5672
            };

            var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(channelName,true,false,false);

            var json = JsonConvert.SerializeObject(message);
            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(exchange: "", routingKey: channelName, body: body);
        }
    }
}
