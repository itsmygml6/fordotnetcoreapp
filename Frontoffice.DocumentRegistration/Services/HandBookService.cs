﻿using APEX.Data.Models;
using CRM.Common;
using Newtonsoft.Json;

namespace Frontoffice.DocumentRegistration.Services
{
    public class HandBookService
    {
        private readonly AppConfig _appConfig;

        public HandBookService(AppConfig appConfig)
        {
            _appConfig = appConfig;
        }

        public async Task<Bank>? GetBankByIdAsync(HttpClient _httpClient, long bankId)
        {
            var response = await _httpClient.GetAsync($"{_appConfig.BaseUrlHandbook}api/bank/{bankId}");
            
            return response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<Bank>(await response.Content.ReadAsStringAsync()) : null;
        }
    }
}
