﻿namespace Frontoffice.DocumentSigning.DTO
{
    public class SignedDocumentResponseDto
    {
        public string sign { get; set; }
        public Document document { get; set; }
    }

    public class Document
    {
        public string requestId { get; set; }
        public string id { get; set; }
        public string shortId { get; set; }
        public string clientId { get; set; }
        public string pin { get; set; }
        public string state { get; set; }
        public Attributes attributes { get; set; }
        public int documentLevel { get; set; }
        public string documentType { get; set; }
        public string signType { get; set; }
    }
    public class Attributes
    {
        public string mode { get; set; }
    }
}
