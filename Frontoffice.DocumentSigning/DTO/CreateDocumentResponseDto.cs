﻿namespace Frontoffice.DocumentSigning.DTO
{
    public class CreateDocumentResponseDto
    {
        public string requestId { get; set; }
        public string shortId { get; set; }
        public string id { get; set; }
        public string pin { get; set; }
        public string documentUrl { get; set; }
        public object shortLink { get; set; }
        public object linkExpiredAt { get; set; }

        /// <summary>
        /// if error
        /// </summary>
        public string path { get; set; }
        public DateTime timestamp { get; set; }
        public int status { get; set; }
        public string error { get; set; }
        public string message { get; set; }
    }
}
