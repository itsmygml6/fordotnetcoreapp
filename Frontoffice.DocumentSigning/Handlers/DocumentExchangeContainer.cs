﻿namespace Frontoffice.DocumentSigning.Handlers
{
    public class DocumentExchangeContainer<T>
    {
        public string pattern { get; set; }
        public T data { get; set; }
    }
}

/*
DocumentExchangeContainer<SignDocumentData> documentExchangeContainer = new DocumentExchangeContainer<SignDocumentData>();
string doc_data = "{\"pattern\":\"sign - document\",\"data\":{\"requestId\":\"5ef1a6ca - 2a41 - 44b1 - 9bc8 - 34925d43cbf6\",\"id\":\"14660a5a - 4451 - 4bc3 - a013 - 8134b768d75a\",\"shortId\":\"uD9EDebVPCa\",\"clientId\":\"apex\",\"pin\":\"eL59sB41\",\"state\":\"SIGNED\",\"attributes\":{\"mode\":\"dev\"},\"documentLevel\":1,\"documentType\":\"FTL_JSON\",\"signType\":\"EIMZO\"}}";
string upd_data_err = "{\"pattern\":\"update - document\",\"data\":{\"path\":\" / api / v2 / document / create\",\"timestamp\":\"2022 - 03 - 27T19: 00:13.702255167\",\"status\":400,\"error\":\"bad - request.DocumentPayload.duplicatedRequestId\",\"message\":\"Duplicated parametr requestId.\"}}";
string upd_data_success = "{\"pattern\":\"update - document\",\"data\":{\"requestId\":\"6ef1a6ca - 2a41 - 44b1 - 9bc8 - 34925d43cbf6\",\"shortId\":\"4V2UqNsPNna\",\"id\":\"be849c25 - 8053 - 41c7 - 82ba - 766f490169d6\",\"pin\":\"fX45xH27\",\"documentUrl\":\"https://hujjat.uz/document?id=be849c25-8053-41c7-82ba-766f490169d6&pin=fX45xH27\",\"shortLink\":null,\"linkExpiredAt\":null}}";
            
var commandRequest = JsonConvert.DeserializeObject<DocumentExchangeContainer<SignDocumentData>>(doc_data);
var commandRequest1 = JsonConvert.DeserializeObject<DocumentExchangeContainer<UpdateDocumentData>>(upd_data_err);
var commandRequest2 = JsonConvert.DeserializeObject<DocumentExchangeContainer<UpdateDocumentData>>(upd_data_success);

 */