﻿namespace Frontoffice.DocumentSigning.Handlers
{
    public class SignDocumentData
    {
        public string requestId { get; set; }
        
        public string id { get; set; }

        public string shortId { get; set; }

        public string clientId { get; set; }

        public string pin { get; set; }

        public string state { get; set; }

        public int documentLevel { get; set; }

        public string documentType { get; set; }

        public string signType { get; set; }

    }
}
