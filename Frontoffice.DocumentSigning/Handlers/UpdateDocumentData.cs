﻿namespace Frontoffice.DocumentSigning.Handlers
{
    public class UpdateDocumentData
    {
        public string? requestId { get; set; }

        public string? shortId { get; set; }

        public string? id { get; set; }

        public string? pin { get; set; }

        public string? documentUrl { get; set; }

        public string? shortLink { get; set; }

        public string? linkExpiredAt { get; set; }

        public string? path { get; set; }

        public string? timestamp { get; set; }

        public int? status { get; set; }

        public string? error { get; set; }

        public string? message { get; set; }

    }
}
