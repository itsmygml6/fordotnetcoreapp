﻿using Frontoffice.RabbitMQ.Core;
using Frontoffice.DocumentSigning.Services.RabbitMQ.Producer;

namespace Frontoffice.DocumentSigning.Publishers
{
    public class UpdateDocumentPublisher
    {
        private readonly IMessageProducer _messagePublisher;

        public UpdateDocumentPublisher(IMessageProducer messagePublisher)
        {
            _messagePublisher = messagePublisher;
        }

        public void Publish(string updatedDocument)
        {       
            _messagePublisher.SendMessage(RabbitKeys.UPDATE_DOCUMENT_QUEUE, updatedDocument);
        }
    }
}
