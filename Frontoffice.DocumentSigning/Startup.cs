﻿using CRM.Common;
using CRM.Data;
using CRM.Data.Core;
using Frontoffice.RabbitMQ.Core;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerUI;
using Frontoffice.DocumentSigning.Publishers;
using Frontoffice.DocumentSigning.Services;
using Frontoffice.DocumentSigning.Services.RabbitMQ.Producer;
using Microsoft.AspNetCore.Authentication;
using Frontoffice.DocumentSigning.Handlers;

namespace Frontoffice.DocumentSigning
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            HostingEnvironment = environment;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment HostingEnvironment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var config = new AppConfig();
            Configuration.GetSection("Config").Bind(config);
            services.AddSingleton(config);
            services.AddControllersWithViews()
                .AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            );

            services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddControllers();
            services.AddHttpContextAccessor();
            services.AddDbContext<IAppDbContext, AppDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<RabbitClient>();
            services.AddTransient<UpdateDocumentPublisher>();
            services.AddHostedService<NewDocumentProcessingService>();
            services.AddTransient<HujjatService>();
            services.AddSingleton<IMessageProducer, RabbitMQProducer>();



            services.AddAutoMapper(typeof(Startup));
            
            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddAuthentication("BasicAuthentication")
                .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>
                ("BasicAuthentication", null);

            //services.AddAuthentication(options =>
            //{
            //    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            //})
            //    .AddJwtBearer(options =>
            //    {
            //        options.MetadataAddress = $"{config.AuthUrl}/auth/realms/{config.Realm}/.well-known/openid-configuration";
            //        options.RequireHttpsMetadata = false;
            //        options.IncludeErrorDetails = true;

            //        options.TokenValidationParameters = new TokenValidationParameters
            //        {
            //            ValidateLifetime = true,
            //            ValidateAudience = false,
            //            ValidateIssuerSigningKey = true,
            //            ValidateTokenReplay = true,
            //            ValidateActor = false,
            //            ValidateIssuer = true,
            //        };
            //    });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = $"{config.Title} API",
                    Version = $"v1"
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                    new string[] {}
                    }
                });

            });

            services.AddHttpClient();

            services.AddAuthorization();
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", builder => builder.SetIsOriginAllowed(a => true).AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            });


            services.AddHealthChecks()
                .AddNpgSql(Configuration.GetConnectionString("DefaultConnection"));

        }

        public void Configure(IApplicationBuilder app, AppConfig config)
        {
            //app.UseAppException();

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseCors("AllowAll");
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("health", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });

                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.DisplayRequestDuration();
                c.DocExpansion(DocExpansion.None);
                c.ShowExtensions();
                c.EnableValidator();

                c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{config.Title} API V1");
            });
        }
    }
}
