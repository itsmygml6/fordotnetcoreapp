﻿using Frontoffice.DocumentSigning.DTO;
using Frontoffice.DocumentSigning.Services.RabbitMQ.Producer;
using Frontoffice.RabbitMQ.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Frontoffice.DocumentSigning.Controllers
{

    [Route("prod/callback")]
    [Route("dev/callback")]
    [ApiController]    
    public class CallbackController : ControllerBase
    {
        private readonly IMessageProducer _messagePublisher;

        public CallbackController(IMessageProducer messagePublisher)
        {
            _messagePublisher = messagePublisher;
        }

        // POST api/<CallbackController>
        [HttpPost]
        [Authorize]
        public void Post([FromBody] SignedDocumentResponseDto signedDocument)
        {
            var message = JsonConvert.SerializeObject(signedDocument.document);
            _messagePublisher.SendMessage(RabbitKeys.SIGNED_DOCUMENT_QUEUE, message);
        }
    }
}