﻿using CRM.Common;
using CRM.Common.Rest;
using CRM.Data.Models;
using Frontoffice.DocumentSigning.DTO;
using Newtonsoft.Json;
using System.Text;

namespace Frontoffice.DocumentSigning.Services
{
    public class HujjatService
    {
        private readonly AppConfig _appConfig;
        private readonly HttpClient _httpClient;
        public HujjatService(AppConfig appConfig, HttpClient httpClient) 
        {
            _appConfig = appConfig;
            _httpClient = httpClient;
        }

        public async Task<CreateDocumentResponseDto> Create(string document)
        {
            _httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _appConfig.HujjatClientToken);

            //var documentString = JsonConvert.SerializeObject(document);

            //var documentContent = new StringContent(document, Encoding.UTF8);//, "application/json"); //working code

            byte[] messageBytes = System.Text.Encoding.UTF8.GetBytes(document);
            var content = new ByteArrayContent(messageBytes);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await _httpClient.PostAsync($"https://hujjat.uz/api/v2/document/create", content);

            //var responceContent = response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<ApiResponse>(await response.Content.ReadAsStringAsync()).Data : null;

            var resp = JsonConvert.DeserializeObject<CreateDocumentResponseDto>(await response.Content.ReadAsStringAsync());//JsonConvert.DeserializeObject<CreateDocumentErrorResponce>(await response.Content.ReadAsStringAsync());

            return resp;
        }
    }
}
