﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using Newtonsoft.Json;
using CRM.Common;
using Frontoffice.RabbitMQ.Core;
using Frontoffice.DocumentSigning.Handlers;
using CRM.Data.Models;
using Frontoffice.DocumentSigning.Publishers;

namespace Frontoffice.DocumentSigning.Services
{
    public class NewDocumentProcessingService : IHostedService
    {
        private readonly IModel _channel;
        private readonly ILogger<NewDocumentProcessingService> _logger;
        private readonly HujjatService _hujjatService;
        private readonly UpdateDocumentPublisher _updatePublisher;

        public NewDocumentProcessingService(AppConfig config, ILogger<NewDocumentProcessingService> logger, RabbitClient client, HujjatService hujjatService, UpdateDocumentPublisher updatePublisher)
        {
            _logger = logger;
            _channel = client.CreateChannel(config.RabbitMQConnection, config.RabbitMQUser, config.RabbitMQPassword);
            _hujjatService = hujjatService;
            _updatePublisher = updatePublisher;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {

            _channel.ExchangeDeclare(RabbitKeys.NEW_DOCUMENT_QUEUE, ExchangeType.Direct);
            _channel.QueueDeclare(RabbitKeys.NEW_DOCUMENT_QUEUE, true, false, false);

            _channel.QueueBind(RabbitKeys.NEW_DOCUMENT_QUEUE, RabbitKeys.NEW_DOCUMENT_QUEUE, "");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (sender, e) =>
            {
                await Task.Factory.StartNew(async (body) =>
                {
                    var message = Encoding.UTF8.GetString((byte[])body);
                    var commandRequest = JsonConvert.DeserializeObject<DocumentBody>(message);

                    if (message != String.Empty)
                    {
                        var createdDocument = await _hujjatService.Create(message);


                        
                        if (createdDocument.documentUrl != null)
                        {
                            var createdDocumentResponse = JsonConvert.SerializeObject(createdDocument);

                            _updatePublisher.Publish(createdDocumentResponse);
                        }
                        else
                        {
                            var createdDocumentResponse = JsonConvert.SerializeObject(createdDocument);
                            _logger.LogError(createdDocumentResponse);
                        }

                    }
                }, e.Body.ToArray(), cancellationToken);
            };

            _channel.BasicConsume(RabbitKeys.NEW_DOCUMENT_QUEUE, true, consumer);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _channel.Close();

            return Task.CompletedTask;
        }
    }
}
